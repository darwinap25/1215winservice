﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// Accounts
    /// </summary>
    [Serializable]
    public partial class Accounts
    {
        public Accounts()
        { }
        #region Model
        private Acc _Acc;

        [XmlElement("Acc")]
        public Acc Acc
        {
            get
            {
                if (_Acc == null)
                {
                    _Acc = new Acc();
                }
                return _Acc;
            }
            set { _Acc = value; }
        }

        #endregion Model
    }
}
