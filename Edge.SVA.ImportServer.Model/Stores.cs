﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// Stores
    /// </summary>
    [Serializable]
    public partial class Stores
    {
        public Stores()
        { }
        #region Model
        private List<Store> store;

        [XmlElement("Store")]
        public List<Store> Store
        {
            get
            {
                if (store == null)
                {
                    store = new List<Store>();
                }
                return store;
            }
            set { store = value; }
        }

        #endregion Model
    }
}
