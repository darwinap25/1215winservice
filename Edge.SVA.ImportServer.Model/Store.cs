﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// Store
    /// </summary>
    [Serializable]
    public partial class Store
    {
        public Store()
        { }
        #region Model
        private string _id;
        private string _storetypeid;
        private string _ishomestore;

        /// <summary>
        /// 主键
        /// </summary>
        [XmlAttribute("Id")]
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("StoreTypeId")]
        public string StoreTypeId
        {
            set { _storetypeid = value; }
            get { return _storetypeid; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("IsHomeStore")]
        public string IsHomeStore
        {
            set { _ishomestore = value; }
            get { return _ishomestore; }
        }
        #endregion Model
    }
}
