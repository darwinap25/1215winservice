﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// Retailer
    /// </summary>
    [Serializable]
    public partial class Retailer
    {
        public Retailer()
        { }
        #region Model
        private string _id;
        private List<HouseHold> household;
        //private HouseHoldMovements houseHoldmovements;
        private List<HouseHoldMovements> houseHoldmovements;       

        /// <summary>
        /// 主键
        /// </summary>
        [XmlAttribute("Id")]
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        [XmlElement("HouseHold")]
        public List<HouseHold> HouseHold
        {
            get
            {
                if (household == null)
                {
                    household = new List<HouseHold>();
                }
                return household;
            }
            set { household = value; }
        }

        //[XmlElement("HouseHoldMovements")]
        //public HouseHoldMovements HouseHoldMovements
        //{
        //    get { return houseHoldmovements; }
        //    set { houseHoldmovements = value; }
        //}

        //[XmlElement("HouseHoldMovements")]
        //public List<HouseHoldMovements> HouseHoldMovements
        //{
        //    get { return houseHoldmovements;  }
        //    get
        //    {
        //        if (houseHoldmovements == null)
        //        {
        //            houseHoldmovements = new List<HouseHoldMovements>();
        //        }
        //        return houseHoldmovements;
        //    }
        //    set { houseHoldmovements = value; }
        //}


        private List<HouseHoldArray> householdarray;

        [XmlElement("HouseHoldMovements")]
        public List<HouseHoldArray> HouseHoldArray
        {
            get
            {
                if (householdarray == null)
                {
                    householdarray = new List<HouseHoldArray>();
                }
                return householdarray;
            }
            set { householdarray = value; }
        }
     
        #endregion Model
    }
}
