﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// LoyaltyInfo
    /// </summary>
    [Serializable]
    public partial class LoyaltyInfo
    {
        public LoyaltyInfo()
        { }
        #region Model
        private string _cardid;
        private string _serverdate;  //日期格式; 2015-01-29
        private Accounts _Accounts;

        /// <summary>
        /// 主键
        /// </summary>
        [XmlAttribute("CardID")]
        public string CardID
        {
            set { _cardid = value; }
            get { return _cardid; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("ServerDate")]
        public string ServerDate
        {
            set { _serverdate = value; }
            get { return _serverdate; }
        }

        [XmlElement("Accounts")]
        public Accounts Accounts
        {
            get
            {
                if (_Accounts == null)
                {
                    _Accounts = new Accounts();
                }
                return _Accounts;
            }
            set { _Accounts = value; }
        }
        #endregion Model
    }
}
