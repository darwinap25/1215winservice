﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// MemberAttributes
    /// </summary>
    [Serializable]
    public partial class MemberAttributes
    {
        public MemberAttributes()
        { }
        #region Model
        private List<Attribute> attribute;

        [XmlElement("Attribute")]
        public List<Attribute> Attribute
        {
            get
            {
                if (attribute == null)
                {
                    attribute = new List<Attribute>();
                }
                return attribute;
            }
            set { attribute = value; }
        }

        #endregion Model
    }
}
