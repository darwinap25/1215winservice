﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// Customer
    /// </summary>
    [Serializable]
    public partial class Customer
    {
        public Customer()
        { }
        #region Model
        private string _msgtype;
        private string _lpever;
        private string _storeid;
        private string _posid;
        private string _cashierid;
        private string _tickettotal;  //浮点数格式： 0.00
        private string _homestore;
        private string _filesource;
        private string _businessdate; //日期格式; 2015-01-29
        private string _startdatetime;  //日期格式; 2015-01-29T12:12:12
        private string _transid;
        private string _retailerid;
        //private LoyaltyInfo _LoyaltyInfo;
        private List<LoyaltyInfo> _LoyaltyInfo;

        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("MsgType")]
        public string MsgType
        {
            set { _msgtype = value; }
            get { return _msgtype; }
        }
        [XmlAttribute("LPEVer")]
        public string LPEVer
        {
            set { _lpever = value; }
            get { return _lpever; }
        }
        [XmlAttribute("StoreID")]
        public string StoreID
        {
            set { _storeid = value; }
            get { return _storeid; }
        }
        [XmlAttribute("PosID")]
        public string PosID
        {
            set { _posid = value; }
            get { return _posid; }
        }
        [XmlAttribute("CashierID")]
        public string CashierID
        {
            set { _cashierid = value; }
            get { return _cashierid; }
        }
        [XmlAttribute("TicketTotal")]
        public string TicketTotal
        {
            set { _tickettotal = value; }
            get { return _tickettotal; }
        }
        [XmlAttribute("HomeStore")]
        public string HomeStore
        {
            set { _homestore = value; }
            get { return _homestore; }
        }
        [XmlAttribute("FileSource")]
        public string FileSource
        {
            set { _filesource = value; }
            get { return _filesource; }
        }
        [XmlAttribute("BusinessDate")]
        public string BusinessDate
        {
            set { _businessdate = value; }
            get { return _businessdate; }
        }
        [XmlAttribute("StartDateTime")]
        public string StartDateTime
        {
            set { _startdatetime = value; }
            get { return _startdatetime; }
        }
        [XmlAttribute("TransID")]
        public string TransID
        {
            set { _transid = value; }
            get { return _transid; }
        }
        [XmlAttribute("RetailerId")]
        public string RetailerId
        {
            set { _retailerid = value; }
            get { return _retailerid; }
        }

        [XmlElement("LoyaltyInfo")]
        public List<LoyaltyInfo> LoyaltyInfo
        {
            get
            {
                if (_LoyaltyInfo == null)
                {
                    _LoyaltyInfo = new List<LoyaltyInfo>();
                }
                return _LoyaltyInfo;
            }
            set { _LoyaltyInfo = value; }
        }
        #endregion Model
    }
}
