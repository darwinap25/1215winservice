﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// HouseHold
    /// </summary>
    [Serializable]
    public partial class HouseHold
    {
        public HouseHold()
        { }

        #region Model
        private string _householdexternalid;
        private string _PostalCode;
        private string _Country;
        private string _City;
        private string _Street1;
        private string _HomePhone;
        private string _EMailAddress;
        private string _SendEmail;
        private Members members;
       // private HouseholdSegments householdsegments;

        /// <summary>
        /// 主键
        /// </summary>
        [XmlAttribute("HouseHoldExternalId")]
        public string HouseHoldExternalId
        {
            set { _householdexternalid = value; }
            get { return _householdexternalid; }
        }

        [XmlAttribute("PostalCode")]
        public string PostalCode
        {
            set { _PostalCode = value; }
            get { return _PostalCode; }
        }
        [XmlAttribute("Country")]
        public string Country
        {
            set { _Country = value; }
            get { return _Country; }
        }
        [XmlAttribute("City")]
        public string City
        {
            set { _City = value; }
            get { return _City; }
        }
        [XmlAttribute("Street1")]
        public string Street1
        {
            set { _Street1 = value; }
            get { return _Street1; }
        }
        [XmlAttribute("HomePhone")]
        public string HomePhone
        {
            set { _HomePhone = value; }
            get { return _HomePhone; }
        }
        [XmlAttribute("EMailAddress")]
        public string EMailAddress
        {
            set { _EMailAddress = value; }
            get { return _EMailAddress; }
        }
        [XmlAttribute("SendEmail")]
        public string SendEmail
        {
            set { _SendEmail = value; }
            get { return _SendEmail; }
        }
        [XmlElement("Members")]
        public Members Members
        {
            get
            {
                if (members == null)
                {
                    members = new Members();
                }
                return members;
            }
            set { members = value; }
        }

        //[XmlElement("HouseholdSegments")]
        //public HouseholdSegments HouseholdSegments
        //{
        //    get
        //    {
        //        if (householdsegments == null)
        //        {
        //            householdsegments = new HouseholdSegments();
        //        }
        //        return householdsegments;
        //    }
        //    set { householdsegments = value; }
        //}
        #endregion Model
    }
}
