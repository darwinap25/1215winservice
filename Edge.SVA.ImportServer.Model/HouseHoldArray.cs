﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;


namespace Edge.SVA.ImportServer.Model
{
    [Serializable]
    public partial class HouseHoldArray
    {
        public HouseHoldArray()
        {        
        }

        #region Model
        //private string _id;
        //private List<HouseHold> household;
        ////private HouseHoldMovements houseHoldmovements;
        private List<HouseHoldMovements> houseHoldmovements;

        /////// <summary>
        /////// 主键
        /////// </summary>
        ////[XmlAttribute("Id")]
        ////public string Id
        ////{
        ////    set { _id = value; }
        ////    get { return _id; }
        ////}
        //[XmlElement("HouseHold")]
        //public List<HouseHold> HouseHold
        //{
        //    get
        //    {
        //        if (household == null)
        //        {
        //            household = new List<HouseHold>();
        //        }
        //        return household;
        //    }
        //    set { household = value; }
        //}
      
        ////[XmlElement("HouseHoldMovements")]
        ////public HouseHoldMovements HouseHoldMovements
        ////{
        ////    get { return houseHoldmovements;  }
        ////    set { houseHoldmovements = value; }
        ////}

        [XmlElement("HouseHoldArray")]
        public List<HouseHoldMovements> HouseHoldMovements
        {
            //get { return houseHoldmovements;  }
            get
            {
                if (houseHoldmovements == null)
                {
                    houseHoldmovements = new List<HouseHoldMovements>();
                }
                return houseHoldmovements;
            }
            set { houseHoldmovements = value; }
        }
        #endregion Model
    }
}
