﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// MemberAttribute
    /// </summary>
    [Serializable]
    public partial class Attribute
    {
        public Attribute()
        { }
        #region Model
        private string _id;
        private string _value;

        /// <summary>
        /// 主键
        /// </summary>
        [XmlAttribute("Id")]
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("Value")]
        public string Value
        {
            set { _value = value; }
            get { return _value; }
        }
        #endregion Model
    }
}
