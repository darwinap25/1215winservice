﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// HouseHoldMerge
    /// </summary>
    [Serializable]
    public partial class HouseHoldMerge
    {
        public HouseHoldMerge()
        { }
        #region Model
        private string _destinationhouseholdexternalid;
        private string _sourcehouseholdexternalid;

        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("DestinationHouseHoldExternalId")]
        public string DestinationHouseHoldExternalId
        {
            set { _destinationhouseholdexternalid = value; }
            get { return _destinationhouseholdexternalid; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("SourceHouseHoldExternalId")]
        public string SourceHouseHoldExternalId
        {
            set { _sourcehouseholdexternalid = value; }
            get { return _sourcehouseholdexternalid; }
        }
        #endregion Model
    }
}
