﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Xml.Serialization;

//namespace Edge.SVA.ImportServer.Model
//{
//    /// <summary>
//    /// Members
//    /// </summary>
//    [Serializable]
//    public partial class Members
//    {
//        public Members()
//        { }
//        #region Model
//        private Member member;

//        [XmlElement("Member")]
//        public Member Member
//        {
//            get
//            {
//                if (member == null)
//                {
//                    member = new Member();
//                }
//                return member;
//            }
//            set { member = value; }
//        }

//        #endregion Model
//    }
//}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
     //<summary>
     //Members
     //</summary>
    [Serializable]
    public partial class Members
    {
        public Members()
        { }
        #region Model
        private List<Member> member;

        [XmlElement("Member")]
        public List<Member> Member
        {
            get
            {
                if (member == null)
                {
                    member = new List<Member>();
                }
                return member;
            }
            set { member = value; }
        }

        #endregion Model
    }
}