﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// account
    /// </summary>
    [Serializable]
    public partial class Acc
    {
        public Acc()
        { }
        #region Model
        private string _id;
        private string _earnvalue;   //浮点数格式： 0.00
        private string _rdmvalue;    //浮点数格式： 0.00
        private string _openbalance;  //浮点数格式： 0.00
        private string _retailerid;

        /// <summary>
        /// 主键
        /// </summary>
        [XmlAttribute("ID")]
        public string ID
        {
            set { _id = value; }
            get { return _id; }
        }
        [XmlAttribute("EarnValue")]
        public string EarnValue
        {
            set { _earnvalue = value; }
            get { return _earnvalue; }
        }
        [XmlAttribute("RdmValue")]
        public string RdmValue
        {
            set { _rdmvalue = value; }
            get { return _rdmvalue; }
        }
        [XmlAttribute("OpenBalance")]
        public string OpenBalance
        {
            set { _openbalance = value; }
            get { return _openbalance; }
        }
        [XmlAttribute("RetailerId")]
        public string RetailerId
        {
            set { _retailerid = value; }
            get { return _retailerid; }
        }
        #endregion Model
    }
}
