﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// Accounts
    /// </summary>
    [Serializable]
    [XmlRoot("Root")]
    public partial class Root
    {
        public Root()
        { }
        #region Model
        private Customer _Customer;

        [XmlElement("Customer")]
        public Customer Customer
        {
            get
            {
                if (_Customer == null)
                {
                    _Customer = new Customer();
                }
                return _Customer;
            }
            set { _Customer = value; }
        }

        #endregion Model
    }
}
