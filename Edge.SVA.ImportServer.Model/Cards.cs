﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// Retailer
    /// </summary>
    [Serializable]
    public partial class Cards
    {
        public Cards()
        { }
        #region Model
        private List<Card> card;

        [XmlElement("Card")]
        public List<Card> Card
        {
            get
            {
                if (card == null)
                {
                    card = new List<Card>();
                }
                return card;
            }
            set { card = value; }
        }

        #endregion Model
    }
}
