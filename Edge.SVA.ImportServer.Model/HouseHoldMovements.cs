﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// HouseHoldMovements
    /// </summary>
    [Serializable]
    public partial class HouseHoldMovements
    {
        public HouseHoldMovements()
        { }
        #region Model
        private HouseHoldMerge householdmerge;
        //private List<HouseHoldMerge> householdmerge;

        [XmlElement("HouseHoldMerge")]
        public HouseHoldMerge HouseHoldMerge
        {
                        
            get { return householdmerge; }
            set { householdmerge = value; }
        }

        //[XmlElement("HouseHoldMerge")]
        //public List<HouseHoldMerge> HouseHoldMerge
        //{
        //    get
        //    {
        //        if (householdmerge == null)
        //        {
        //            householdmerge = new List<HouseHoldMerge>();
        //        }
        //        return householdmerge;
        //    }
        //    set { householdmerge = value; }
        //}
        #endregion Model
    }
}
