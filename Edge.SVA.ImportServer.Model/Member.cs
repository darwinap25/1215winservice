﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// Member
    /// </summary>
    [Serializable]
    public partial class Member
    {
        public Member()
        { }
        #region Model

    
        private string _memberexternalid;
        private string _lastname;
        private string _middleinitial;
        private string _ismainmember;
        private string _firstname;
        private string _birthdate;   //日期格式; 2015-01-29
        private string _mobilephonenumber;
        private string _workphonenumber;
        private string _gender;     // "1"
        private string _startdate;  //日期格式; 2015-01-29T00:00:00
        private string _effectivedate; //日期格式; 2015-01-29T00:00:00
        private string _memberstatus;  // "1"
        private Cards cards;
        private Stores stores;
        private MemberAttributes memberattributes;

        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("MemberExternalId")]
        public string MemberExternalId
        {
            set { _memberexternalid = value; }
            get { return _memberexternalid; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("FirstName")]
        public string FirstName
        {
            set { _firstname = value; }
            get { return _firstname; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("LastName")]
        public string LastName
        {
            set { _lastname = value; }
            get { return _lastname; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("MiddleInitial")]
        public string MiddleInitial
        {
            set { _middleinitial = value; }
            get { return _middleinitial; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("IsMainMember")]
        public string IsMainMember
        {
            set { _ismainmember = value; }
            get { return _ismainmember; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("BirthDate")]
        public string BirthDate
        {
            set { _birthdate = value; }
            get { return _birthdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("MobilePhoneNumber")]
        public string MobilePhoneNumber
        {
            set { _mobilephonenumber = value; }
            get { return _mobilephonenumber; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("WorkPhoneNumber")]
        public string WorkPhoneNumber
        {
            set { _workphonenumber = value; }
            get { return _workphonenumber; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("Gender")]
        public string Gender
        {
            set { _gender = value; }
            get { return _gender; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("StartDate")]
        public string StartDate
        {
            set { _startdate = value; }
            get { return _startdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("EffectiveDate")]
        public string EffectiveDate
        {
            set { _effectivedate = value; }
            get { return _effectivedate; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("MemberStatus")]
        public string MemberStatus
        {
            set { _memberstatus = value; }
            get { return _memberstatus; }
        }

        [XmlElement]
        public Cards Cards
        {
            get
            {
                if (cards == null)
                {
                    cards = new Cards();
                }
                return cards;
            }
            set { cards = value; }
        }

        /// <summary>
        /// Make sure the list of Cards has any content
        /// </summary>
        public bool ShouldSerializeCards()
        {
            return !(Cards.Card.Count == 0);
        }

        [XmlElement("Stores")]
        public Stores Stores
        {
            get { return stores; }
            set { stores = value; }
        }

        [XmlElement("MemberAttributes")]
        public MemberAttributes MemberAttributes
        {
            get { return memberattributes;}
            set { memberattributes = value; }
        }

        #endregion Model
    }
}
