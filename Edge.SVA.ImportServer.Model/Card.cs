﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    /// <summary>
    /// 卡
    /// </summary>
    [Serializable]
    public partial class Card
    {
        public Card()
        { }
        #region Model
        private string _id;
        private string _cardstatus;
        private string _issuedate;   //日期格式; 2015-01-29T00:00:00
        private string _expirationdate;  //日期格式; 2015-01-29T00:00:00
        private string _effectivedate;  //日期格式; 2015-01-29T00:00:00

        /// <summary>
        /// 主键
        /// </summary>
        [XmlAttribute("Id")]
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        [XmlAttribute("CardStatus")]
        public string CardStatus
        {
            set { _cardstatus = value; }
            get { return _cardstatus; }
        }
        [XmlAttribute("IssueDate")]
        public string IssueDate
        {
            set { _issuedate = value; }
            get { return _issuedate; }
        }
        [XmlAttribute("ExpirationDate")]
        public string ExpirationDate
        {
            set { _expirationdate = value; }
            get { return _expirationdate; }
        }
        [XmlAttribute("EffectiveDate")]
        public string EffectiveDate
        {
            set { _effectivedate = value; }
            get { return _effectivedate; }
        }
        #endregion Model
    }
}
