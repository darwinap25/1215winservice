﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model
{
    [Serializable]
    public partial class HouseHoldSegments
    {
        public HouseHoldSegments()
        { }

        private List<Segment> segment;
        

        [XmlElement("Segment")]
        public List<Segment> Segment
        {
            get
            {
                if (segment == null)
                {
                    segment = new List<Segment>();
                }
                return segment;
            }
            set { segment = value; }
        }


    }
}
