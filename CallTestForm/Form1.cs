﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using Edge.SVA.ImportMain;


namespace CallTestForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            textBox2.Text = "C:\\Working\\Edge.SVA.ImportServer\\ExportPath\\";
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(textBox1.Text) )
            {
                MessageBox.Show("No file to open");
            }
            else
            {
                this.richTextBox1.Text = "test ok";
                myclass a = new myclass();
                a.p1 = "good";
                a.p2 = "ds";
                a.p3 = "dssss";
                XmlSerializer.SaveToXml("C:\\Working\\test.xml", a, a.GetType(), "root");
                
                //XmlSerializer.LoadFromXml("test.xml", a.GetType());
                this.richTextBox1.Text = a.ToString();
            }           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //myclass a = new myclass();
            //XmlSerializer.LoadFromXml("test.xml", a.GetType());
            //this.richTextBox1.Text = a.ToString();
            //string LoyaltyDBConnStr = "server=172.16.64.25;database=Loyalty_Robinsons_Staging;uid=sa;pwd=123qwE;Connect Timeout=600";
            string LoyaltyDBConnStr = "server=10.81.2.81;database=Loyalty_Robinsons_PreProd;uid=TAPmemberimportpreprod;pwd=Abcd@1234;Connect Timeout=600";
            string SVADBConnStr = "server=PHTEC001\\PHTEC001DAP;database=SVA321;uid=sa;pwd=Darwin_25;Connect Timeout=6000";
                                        //"server=172.16.192.59;database=SVA321;uid=sa;pwd=1234@abcd;Connect Timeout=600"; 
                                        //"server=172.16.64.24\\SQLEXPRESS;database=SVA321EGC;uid=sa;pwd=1234qweR;Connect Timeout=600";

           // ImportFile im = new ImportFile(LoyaltyDBConnStr, SVADBConnStr);
            //im.ReadFile("C:\\Working\\Edge.SVA.ImportServer\\ImportPath\\test.csv", "C:\\Working\\Edge.SVA.ImportServer\\ExportPath\\");



            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                textBox1.Text = openFileDialog1.FileName;
                
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = folderBrowserDialog1.SelectedPath + "\\";
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            string SVADBConnStr = "server=PHTEC001\\PHTEC001DAP;database=SVA321;uid=sa;pwd=Darwin_25;Connect Timeout=6000";

            BusinessLogic bll = new BusinessLogic(SVADBConnStr);

            DateTime date1 = DateTime.Parse("02/4/2016"); //DateTime.Now.Date;

           // MessageBox.Show(bll.SetDateToEOM(date1).ToString());

        }
    }

    public class myclass
    {
        public string p1;
        public string p2;
        public string p3;
    }

    public static class XmlSerializer
    {
        public static void SaveToXml(string filePath, object sourceObj, Type type, string xmlRootName)
        {
            if (!string.IsNullOrEmpty(filePath) && sourceObj != null)
            {
                type = type != null ? type : sourceObj.GetType();

                using (StreamWriter writer = new StreamWriter(filePath, false, Encoding.ASCII))
                {
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                    //Add an empty namespace and empty value  
                    ns.Add("", "");

                    System.Xml.Serialization.XmlSerializer xmlSerializer = string.IsNullOrEmpty(xmlRootName) ?
                        new System.Xml.Serialization.XmlSerializer(type) :
                        new System.Xml.Serialization.XmlSerializer(type, new XmlRootAttribute(xmlRootName));
                    xmlSerializer.Serialize(writer, sourceObj, ns);
                }
            }
        }

        public static object LoadFromXml(string filePath, Type type)
        {
            object result = null;

            if (File.Exists(filePath))
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(type);
                    result = xmlSerializer.Deserialize(reader);
                }
            }

            return result;
        }


    }
}
