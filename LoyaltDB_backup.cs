﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Edge.SVA.ImportMain
{
    public class LoyaltDB
    {
        SqlConnection sqlconn;
       //     sqlconn.Close();                           //关闭连接
       //     SqlConnection.ClearPool(sqlconn);          //清空conn连接池
       //     SqlConnection.ClearAllPools();          //清空连接池
        string ConnStr = "";
        public LoyaltDB(string connstr)
        {
            ConnStr = connstr;
            SqlConnection sqlconn = new SqlConnection(connstr);
            sqlconn.Open();
        }
        public string GetMemberInternalKey(string CardNumber, ref DateTime ExpirationDate)
        {
            //SqlConnection sqlconn = new SqlConnection(ConnStr);     
            //sqlconn.Open();
            string MemberInternalKey = "";
            string queryString = "Select ClubCardId,MemberInternalKey, ExpirationDate from CRM_Clubcard where ClubCardId = '" + CardNumber + "'";
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet ds = new DataSet();
            adapter.SelectCommand = new SqlCommand(queryString, sqlconn);
            adapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                MemberInternalKey = ds.Tables[0].Rows[0]["MemberInternalKey"].ToString();
                ExpirationDate = DateTime.Parse(ds.Tables[0].Rows[0]["ExpirationDate"].ToString());
                ExpirationDate = ExpirationDate.AddYears(2);
            }
            //sqlconn.Close();
            //SqlConnection.ClearPool(sqlconn);          //清空conn连接池
            //SqlConnection.ClearAllPools();          //清空连接池
            return MemberInternalKey;
        }
        public string GetBuyingUnitInternalKey(string CardNumber, string MemberInternalKey)
        {
            //SqlConnection sqlconn = new SqlConnection(ConnStr);     
            //sqlconn.Open();
            string BuyingUnitInternalKey = "";
            string queryString = "Select BuyingUnitInternalKey from CRM_Member where MemberInternalKey='" + MemberInternalKey + "' and ExternalMemberKey='" + CardNumber + "'";
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet ds = new DataSet();
            adapter.SelectCommand = new SqlCommand(queryString, sqlconn);
            adapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                BuyingUnitInternalKey = ds.Tables[0].Rows[0]["BuyingUnitInternalKey"].ToString();
            }
            //sqlconn.Close();
            //SqlConnection.ClearPool(sqlconn);          //清空conn连接池
            //SqlConnection.ClearAllPools();          //清空连接池
            return BuyingUnitInternalKey;
        }

        public string GetExternalBuyingUnit(string CardNumber, string BuyingUnitInternalKey)
        {
            //SqlConnection sqlconn = new SqlConnection(ConnStr);     
            //sqlconn.Open();
            string ExternalBuyingUnit = "";
            string queryString = "Select ExternalBuyingUnit from CRM_BuyingUnit where BuyingUnitInternalKey='" + BuyingUnitInternalKey + "'";
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet ds = new DataSet();
            adapter.SelectCommand = new SqlCommand(queryString, sqlconn);
            adapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ExternalBuyingUnit = ds.Tables[0].Rows[0]["ExternalBuyingUnit"].ToString();
            }
            //sqlconn.Close();
            //SqlConnection.ClearPool(sqlconn);          //清空conn连接池
            //SqlConnection.ClearAllPools();          //清空连接池
            return ExternalBuyingUnit;
        }
    }
}
