﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Common
{
    public class Logging
    {

        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(
                                System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        AppConfig config = AppConfig.GetSingleton();

        public Logging()
        {

        }

        public void WriteOperationLog(string methodname, string message)
        {
            logger.Info(methodname + ": "+ message);
        }


    }
}
