﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;
using System.Xml;

namespace Common
{
    public static class XmlSerializer
    {
        public static void SaveToXml(string filePath, object sourceObj, Type type, string xmlRootName)
        {
            if (!string.IsNullOrEmpty(filePath) && sourceObj != null)
            {
                type = type != null ? type : sourceObj.GetType();

                using (StreamWriter writer = new StreamWriter(filePath, false, Encoding.GetEncoding("ISO-8859-1")))
                {
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                    //Add an empty namespace and empty value  
                    if (xmlRootName != "Root")
                        ns.Add("", "");
//                        ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");

                    System.Xml.Serialization.XmlSerializer xmlSerializer = string.IsNullOrEmpty(xmlRootName) ?
                        new System.Xml.Serialization.XmlSerializer(type) :
                        new System.Xml.Serialization.XmlSerializer(type, new XmlRootAttribute(xmlRootName));
                                      
                   xmlSerializer.Serialize(writer, sourceObj, ns);
                   
                    
                }
            }
        }

        public static void cleanXML(string filePath)
        {
            XmlDocument xmlDoc = new XmlDocument();

            try
            {
                xmlDoc.Load(filePath);
                //delete <HouseHoldArray> and </HouseHoldArray> elements from XML file
                deleteNode(filePath, xmlDoc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void deleteNode(string fileName, XmlDocument xmlDoc)
        {
            XmlElement _xmlElement = xmlDoc.DocumentElement;
            string x = _xmlElement.InnerXml.Replace("<HouseHoldArray>", "");
            x = x.Replace("</HouseHoldArray>", "");
            _xmlElement.InnerXml = x.ToString();
            
            xmlDoc.Save(fileName);
        }
    }
}
