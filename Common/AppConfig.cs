﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Configuration;

namespace Common
{
    public class AppConfig
    {
        private static readonly AppConfig instance = new AppConfig();
        private AppConfig() { InitModel(this); }
        public static AppConfig GetSingleton() { return instance; }

        #region configs
        private string svaconnectionString;
        public string SVAConnectionString
        {
            get { return svaconnectionString; }
            set { svaconnectionString = value; }
        }

        private string loyaltyconnectionString;
        public string LoyaltyConnectionString
        {
            get { return loyaltyconnectionString; }
            set { loyaltyconnectionString = value; }
        }

        private string importpath;
        public string ImportPath
        {
            get { return importpath; }
            set { importpath = value; }
        }

        private string importpath2;
        public string ImportPath2
        {
            get { return importpath2; }
            set { importpath2 = value; }
        }

        private string importpath3;
        public string ImportPath3
        {
            get { return importpath3; }
            set { importpath3 = value; }
        }

        private string exportpath;
        public string ExportPath
        {
            get { return exportpath; }
            set { exportpath = value; }
        }

        private string exportpath2;
        public string ExportPath2
        {
            get { return exportpath2; }
            set { exportpath2 = value; }
        }

        private string uploadpath;
        public string UploadPath
        {
            get { return uploadpath; }
            set { uploadpath = value; }
        }

        private string pointsexportpath;
        public string PointsexportPath
        {
            get { return pointsexportpath; }
            set { pointsexportpath = value; }
        }

        private string pointsexportpath2;
        public string PointsexportPath2
        {
            get { return pointsexportpath2; }
            set { pointsexportpath2 = value; }
        }

        private string bakpath;
        public string BakPath
        {
            get { return bakpath; }
            set { bakpath = value; }
        }

        private string errorpath;
        public string ErrorPath
        {
            get { return errorpath; }
            set { errorpath = value; }
        }

        private string filefilter;
        public string FileFilter
        {
            get { return filefilter; }
            set { filefilter = value; }
        }

        private bool isschedule;
        public bool IsSchedule
        {
            get { return isschedule; }
            set { isschedule = value; }
        }

        private string ftpprotocol;
        public string FTPProtocol
        {
            get { return ftpprotocol; }
            set { ftpprotocol = value; }
        }

        private string ftphostname;
        public string FTPHostName
        {
            get { return ftphostname; }
            set { ftphostname = value; }
        }

        private string ftpusername;
        public string FTPUserName
        {
            get { return ftpusername; }
            set { ftpusername = value; }
        }

        private string ftppassword;
        public string FTPPassword
        {
            get { return ftppassword; }
            set { ftppassword = value; }
        }

        private string ftpportnumber;
        public string FTPPortNumber
        {
            get { return ftpportnumber; }
            set { ftpportnumber = value; }
        }

        private string ftpimportpath;
        public string FTPImportPath
        {
            get { return ftpimportpath; }
            set { ftpimportpath = value; }
        }

        private string ftpimportpath2;
        public string FTPImportPath2
        {
            get { return ftpimportpath2; }
            set { ftpimportpath2 = value; }
        }

        private string ftpimportpath3;
        public string FTPImportPath3
        {
            get { return ftpimportpath3; }
            set { ftpimportpath3 = value; }
        }

        private string ftpexportpath;
        public string FTPExportPath
        {
            get { return ftpexportpath; }
            set { ftpexportpath = value; }
        }

        private string ftpexportpath2;
        public string FTPExportPath2
        {
            get { return ftpexportpath2; }
            set { ftpexportpath2 = value; }
        }

        private string ftpsshhostkeyfingerprint;
        public string FTPSshHostKeyFingerprint
        {
            get { return ftpsshhostkeyfingerprint; }
            set { ftpsshhostkeyfingerprint = value; }
        }

        private string sendfrom;
        public string SendFrom
        {
            get { return sendfrom; }
            set { sendfrom = value; }
        }

        private string emailpassword;
        public string EmailPassword
        {
            get { return emailpassword; }
            set { emailpassword = value; }
        }

        private string smtphost;
        public string SMTPHost
        {
            get { return smtphost; }
            set { smtphost = value; }
        }

        private string smtpport;
        public string SMTPPort
        {
            get { return smtpport; }
            set { smtpport = value; }
        }

        private string emailrecipients;
        public string EmailReceipients
        {
            get { return emailrecipients; }
            set { emailrecipients = value; }
        }
        #endregion

        #region utils
        private string GetValue(string key)
        {

            if (System.Configuration.ConfigurationManager.AppSettings[key] != null)
            {
                return System.Configuration.ConfigurationManager.AppSettings[key];
            }
            else
            {
                return string.Empty;
            }
        }
        #region InitModelssss

        private void InitModel(object model)
        {
            PropertyInfo[] pros = model.GetType().GetProperties();
            foreach (PropertyInfo pi in pros)
            {
                if (pi.CanWrite)
                {
                    try
                    {
                        pi.SetValue(model, Convert.ChangeType(GetValue(pi.Name), pi.PropertyType), null);
                    }
                    catch (System.Exception ex)
                    {

                    }
                }
            }
        }
        #endregion
        #endregion
    }
}
