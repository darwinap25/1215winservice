﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Data;
using System.IO;

namespace Edge.SVA.ImportService
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new ImportServer() 
                
            };
            ServiceBase.Run(ServicesToRun);
        }

//        static void Main()
//        {
//#if(!DEBUG)
//           ServiceBase[] ServicesToRun;
//           ServicesToRun = new ServiceBase[] 
//       { 
//            new ImportServer()
//       };
//           ServiceBase.Run(ServicesToRun);
//#else
//            ImportServer myServ = new ImportServer();
//            Edge.SVA.ImportServer.ServiceController sc = new Edge.SVA.ImportServer.ServiceController();
//            sc.Start();
//            //here Process is my Service function
//            //that will run when my service onstart is call
//            //you need to call your own method or function name here instead of Process();
//#endif
//        }
   }
}
