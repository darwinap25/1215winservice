﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Edge.SVA.ImportServer;
using Common;

namespace Edge.SVA.ImportService
{
    public partial class ImportServer : ServiceBase
    {
        public ImportServer()
        {
          //  
            InitializeComponent();
            sc.Start();
        }
        Edge.SVA.ImportServer.ServiceController sc = new Edge.SVA.ImportServer.ServiceController();
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void OnStart(string[] args)
        {
            
            AppConfig config = AppConfig.GetSingleton();
//            DatabaseUtil.Factory.SetConnecctionString(config.ConnectionString);
//            DatabaseUtil.Factory.SetDefaultTimeout(600);
           
            sc.Start();
            
        }

        protected override void OnStop()
        {
            sc.Stop();
        }
    }
}
