﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Common;
using Edge.SVA.ImportMain;
using Edge.SVA.ImportMain.Constants;
using System.Threading;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using log4net;

namespace Edge.SVA.ImportServer
{
    public class ActiveRecord
    {
        private DateTime _activetime;
        public DateTime ActiveTime
        {
            set { _activetime = value; }
            get { return _activetime; }
        }
        private string _function;
        public string Function
        {
            set { _function = value; }
            get { return _function; }
        }
        private string _downloadpath;
        public string DownloadPath
        {
            set { _downloadpath = value; }
            get { return _downloadpath; }
        }
        private int _acttimes;
        public int ActTimes
        {
            set { _acttimes = value; }
            get { return _acttimes; }
        }
    }

    //根据订单号在目录下创建子目录，文件放在子目录中，子目录名字就是订单号码
    public class FileRecord
    {
        private string _ordernumber;
        public string OrderNumber
        {
            set { _ordernumber = value; }
            get { return _ordernumber; }
        }
        private string _filename;
        public string FileName
        {
            set { _filename = value; }
            get { return _filename; }
        }
    }

    public class ServiceController
    {
        //private ILog logger = LogManager.GetLogger("ImportFileService");  //在这里使用后，导致不能产生log文件， 未知...
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
       
        FileSystemWatcher watcher;
        System.Threading.Timer m_timer;
        System.Threading.Timer m_timerday;
        AppConfig config;
        string ExportPath = "";
        string ExportPath2 = "";
        string ImportPath = "";
        string ImportPath2 = "";
        //string ImportPath3 = "";     
        string UploadPath = "";
        string BakPath = "";
        string SVADBConnStr = "";
        string FTPProtocol = "";
        string FTPHostName = "";
        string FTPUserName = "";
        string FTPPassword = "";
        string FTPSshHostKeyFingerprint = "";
        string SendFrom = "";
        string EmailPassword = "";
        string SMTPHost = "";
        string SMTPPort = "";
        string EmailReceipients = "";

        int FTPPortNumber = 0;
        bool FIsSchedule = true;
        
        
        static List<ActiveRecord> ActiveTimeList = new List<ActiveRecord>();
        static List<FileRecord> FileRecordList = new List<FileRecord>();

        //        public delegate void DelegateMethod(string csvfile, string exportpath);  //声明了一个Delegate Type
        //        public DelegateMethod delegateMethod;   //声明了一个Delegate对象

        DataTable madt = new DataTable();

        public ServiceController()
        {
            watcher = new FileSystemWatcher();
        //    m_timerday = new Timer(new TimerCallback(OnEveryDayTick), null, 0, 1000 * 60 * 60 * 24);  // 一天一次读取数据库。
            m_timer = new Timer(new TimerCallback(OnTimerTick), null, 0, 1000 * 60);   //一分钟一次
            
            config = AppConfig.GetSingleton();
            ExportPath = config.ExportPath;
            ExportPath2 = config.ExportPath2;
            ImportPath = config.ImportPath;
            ImportPath2 = config.ImportPath2;
            //ImportPath3 = config.ImportPath3;
            FTPProtocol = config.FTPProtocol;
            FTPHostName = config.FTPHostName;
            FTPUserName = config.FTPUserName;
            FTPPassword = config.FTPPassword;
            SendFrom = config.SendFrom;
            EmailPassword = config.EmailPassword;
            SMTPHost = config.SMTPHost;
            SMTPPort = config.SMTPPort;
            EmailReceipients = config.EmailReceipients;
            FTPPortNumber = int.Parse(config.FTPPortNumber);
            FTPSshHostKeyFingerprint = config.FTPSshHostKeyFingerprint;
            
            
            BakPath = config.BakPath;
            SVADBConnStr = config.SVAConnectionString;
            FIsSchedule = config.IsSchedule;
            //delegateMethod = new DelegateMethod(DoImport);      

            watcher.Path = ImportPath;
        }
        public void Start()
        {


            watcher.Path = Path.GetDirectoryName(config.ImportPath);
            watcher.Filter = Path.GetFileName(config.FileFilter);
            watcher.EnableRaisingEvents = true;
            watcher.IncludeSubdirectories = true;

            logger.Info("ImportExport Server - Start - Service start.");
            try
            {
                
                
                logger.Info("Service has been started " + DateTime.Now.ToString());
            }
            catch(Exception ex)
            {
                throw ex;
            }
            SendNotification("ImportExport Server - Start", "Service has been started.");
        }
        public void Stop()
        {
            watcher.EnableRaisingEvents = false;
            SendNotification("ImportExport Server - Stopped", "Service stopped.");
        }

        void watcher_Created(object sender, FileSystemEventArgs e)
        {
   
                logger.Info("ImportExport Server - watcher_Created - watcher_Created start.");
                if (e.ChangeType == WatcherChangeTypes.Created)
                {
                    logger.Info("ImportExport Server - watcher_Created - WatcherChangeTypes.Created.");
                    string fn = "";
                    fn = e.FullPath;
                  
                    int i = 0;
                    while (!IsFileReady(e.FullPath))
                    {
                        if (!File.Exists(e.FullPath))
                            return;
                        Thread.Sleep(100);
                        i = i + 1;
                        if (i > 10)
                            break;
                    }             
                    logger.Info("ImportExport Server - watcher_Created - Do DoImport.");

                    try
                    {
                        logger.Info("ImportExport Server - watcher_Created. Fn:" + fn);
                        int lastp = fn.LastIndexOf("\\");
                        string temp = fn.Substring(0, lastp);
                        logger.Info("ImportExport Server - watcher_Created - temp1 " + temp);
                        lastp = temp.LastIndexOf("\\");
                        temp = fn.Substring(lastp + 1, temp.Length - lastp - 1);
                        logger.Info("ImportExport Server - watcher_Created temp2:" + temp);
                        logger.Info("ImportExport Server - watcher_Created:" + temp + "  " + fn + "  " + ExportPath);
                        //ImportAirline();
                        ImportLostTag();
                        //ImportFoundTag();
                        //DoImport(temp, fn, ExportPath, ExportPath2, PointsExportPath, PointsExportPath2);
                        
                    }
                    catch (System.Exception ex)
                    {
                        logger.Info("ImportExport Server - Error Occured - watcher_Created -Exception:" + ex.InnerException.ToString()); //logs
                        SendNotification("ImportExport Server - Error Occured - watcher_Created", "Exception:" + ex.InnerException.ToString());
                    }
                }
          
        }

        bool IsFileReady(string filename)
        {
            FileInfo fi = new FileInfo(filename);
            FileStream fs = null;
            try
            {
                fs = fi.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                return true;
            }
            catch (IOException)
            {
                return false;
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }

        void watcher_Changed(object sender, FileSystemEventArgs e)
        {

        }


        private void OnWatchedFileChange(object state)
        {

        }


        private void OnTimerTick(object state)
        {
            if (FIsSchedule == true)
            {
               //GetLastActiveTime();

                GetActiveTimeList();

                try
                {
                    
                    SVADB sdb = new SVADB(SVADBConnStr);
                    
                    DoImportExport();
                    
                }
                catch (System.Exception ex)
                {
                   
                    logger.Info("ImportExport Server - Error Occured - OnTimerTick - " + ex.InnerException.ToString());
                }
            }
        }

        private void DoImportExport()
        {
            foreach (ActiveRecord a in ActiveTimeList)
            {
                TimeSpan dt = DateTime.Now.TimeOfDay;
                if (a.ActiveTime.TimeOfDay.ToString().Substring(0, 5) == DateTime.Now.TimeOfDay.ToString().Substring(0, 5))
                {
                    ExportFile ex = new ExportFile(SVADBConnStr);
                    ex.UpdateNextRunSchedule(a.Function.Trim());

                    if (a.Function.Trim() == "Import and Export Files Exchange")
                    {
                        
                        if ((DateTime.Compare(DateTime.Now, a.ActiveTime) >= 0) && (a.ActTimes > 0))
                        {
                            logger.Info("ImportExport Server - DoExport - Start" + a.ActiveTime.ToString() + " Import and Export Files Exchange");
                            
                            
                            this.ExportBaggage();
                            this.ExportMapping();
                            this.ImportAirline();
                            this.ImportLostTag();
                            //this.ImportFoundTag();
                        }

                    }
                    //else if (a.Function.Trim() == "Export Baggage Tag")
                    //{
                    //    ExportFile ex = new ExportFile(SVADBConnStr);
                    //    if ((DateTime.Compare(DateTime.Now, a.ActiveTime) >= 0) && (a.ActTimes > 0))
                    //    {
                    //        try
                    //        {
                    //            logger.Info("ImportExport Server - DoExport - Start" + a.ActiveTime.ToString() + " Baggage Tags");

                    //            int result;
                    //            ex.UpdateNextRunSchedule(a.Function.Trim());
                    //            result = ex.ExportBaggagTags(config.ExportPath);
                    //            //ex.SetCouponsToExported();

                    //            if (result == ReturnCode.SUCCESSFUL)
                    //            {
                    //                ex.SetCouponsToExported();
                    //                logger.Info("ImportExport Server - DoExport - Successful - Export Baggage Tags");


                    //                logger.Info("ImportExport Server - SFTP - Start SFTP Export Baggage Tag");
                    //                WinSCP w = new WinSCP();

                    //                w.TransferBaggageTag();
                    //                logger.Info("ImportExport Server - SFTP - End SFTP Export Baggage Tag");

                    //            }
                    //            else
                    //            {
                    //                logger.Info("ImportExport Server - DoExport - Failed - Export Baggage Tags. Please check log file for details");
                    //            }
                    //        }
                    //        catch (Exception e)
                    //        {
                    //            logger.Info("ImportExport Server - Export Baggage Tags - Exception Occured: Export Baggage Tags." + e.Message.ToString());
                    //            SendNotification("ImportExport Server - Export Baggage Tags", "Exception Occured: Export Baggage Tags." + e.Message.ToString());
                    //        }

                    //    }
                    //    //ex.UpdateNextRunSchedule(a.Function.Trim());
                    //}
                    //else if (a.Function.Trim() == "Export Mapping")
                    //{

                    //    ExportFile ex = new ExportFile(SVADBConnStr);
                    //    if ((DateTime.Compare(DateTime.Now, a.ActiveTime) >= 0) && (a.ActTimes > 0))
                    //    {
                    //        try
                    //        {
                    //            logger.Info("ImportExport Server - DoExport - Start" + a.ActiveTime.ToString() + " Export Mappings");


                    //            int result;
                    //            ex.UpdateNextRunSchedule(a.Function.Trim());
                    //            result = ex.GetMapping(config.ExportPath2);

                    //            if (result == ReturnCode.SUCCESSFUL)
                    //            {

                    //                logger.Info("ImportExport Server - DoExport - Successful - Export Mappings");

                    //                logger.Info("ImportExport Server - SFTP - Start SFTP Export Mappings");
                    //                WinSCP w = new WinSCP();

                    //                w.TransferMapping();
                    //                logger.Info("ImportExport Server - SFTP - End SFTP Export Mappings");
                    //            }
                    //            else
                    //            {
                    //                logger.Info("ImportExport Server - DoExport - Failed - Export Mappings. Please check log file for details");
                    //            }


                    //        }
                    //        catch (Exception e)
                    //        {
                    //            logger.Info("ImportExport Server - Export Mappings - Exception Occured: Export Mappings." + e.Message.ToString());
                    //            SendNotification("ImportExport Server - Export Mappings", "Exception Occured: Export Mappings." + e.Message.ToString());
                    //        }


                    //    }


                    //}

                    //else if (a.Function.Trim() == "Import Airline")
                    //{
                    //    ImportFile im = new ImportFile(SVADBConnStr);
                    //    if ((DateTime.Compare(DateTime.Now, a.ActiveTime) >= 0) && (a.ActTimes > 0))
                    //    {
                    //        try
                    //        {
                    //            im.UpdateNextRunSchedule(a.Function.Trim());
                    //            logger.Info("ImportExport Server - SFTP - tart SFTP Import Airline");
                    //            WinSCP w = new WinSCP();

                    //            w.GetAirline();

                    //            logger.Info("ImportExport Server - SFTP - End SFTP Import Airline");

                    //            foreach (FileRecord f in GetFileList(config.ImportPath))
                    //            {
                    //                try
                    //                {
                    //                    int result;
                    //                    result = im.ImportAirline(f.FileName);
                    //                    string uploadedfile = Path.GetFileName(f.FileName);

                    //                    if (result == ReturnCode.SUCCESSFUL)
                    //                    {

                    //                        System.IO.File.Copy(f.FileName, config.BakPath + uploadedfile);
                    //                        File.Delete(f.FileName);
                    //                        logger.Info("ImportExport Server - Import Airline  Success - File successfully archived . Filename: " + config.BakPath + uploadedfile);

                    //                    }
                    //                    else
                    //                    {
                    //                        System.IO.File.Copy(f.FileName, config.ErrorPath + uploadedfile);
                    //                        File.Delete(f.FileName);
                    //                        logger.Info("ImportExport Server - Import Airline Failed. Please check logs for details - File successfully archived . Filename: " + config.ErrorPath + uploadedfile);
                    //                    }
                    //                }

                    //                catch (Exception ex)
                    //                {
                    //                    logger.Info("ImportExport Server - Import Airline - Exception Occured: Filename: " + f.FileName + ". " + ex.InnerException.ToString().ToString());
                    //                    SendNotification("ImportExport Server - Import Airline", "Exception Occured: Filename: " + f.FileName + ". " + ex.InnerException.ToString().ToString());
                    //                }
                    //            }

                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            logger.Info("ImportExport Server - Import Airline - Exception Occured: SFTP Import Airline." + ex.InnerException.ToString().ToString());
                    //            SendNotification("ImportExport Server - Import Airline", "Exception Occured: SFTP Import Airline." + ex.InnerException.ToString().ToString());

                    //        }

                    //    }

                    //    //im.UpdateNextRunSchedule(a.Function.Trim());

                    //}

                    //else if (a.Function.Trim() == "Import Lost Tag")
                    //{
                    //    ImportFile im = new ImportFile(SVADBConnStr);
                    //    if ((DateTime.Compare(DateTime.Now, a.ActiveTime) >= 0) && (a.ActTimes > 0))
                    //    {
                    //        try
                    //        {
                    //            im.UpdateNextRunSchedule(a.Function.Trim());
                    //            logger.Info("ImportExport Server - SFTP - Start SFTP Import Lost Tags");
                    //            WinSCP w = new WinSCP();

                    //            w.GetLostTags();
                    //            logger.Info("ImportExport Server - SFTP - End SFTP Import Lost Tags");

                    //            foreach (FileRecord f in GetFileList(config.ImportPath2))
                    //            {
                    //                try
                    //                {
                    //                    int result;
                    //                    result = im.ImportLostTag(f.FileName);
                    //                    string uploadedfile = Path.GetFileName(f.FileName);

                    //                    if (result == ReturnCode.SUCCESSFUL)
                    //                    {

                    //                        System.IO.File.Copy(f.FileName, config.BakPath + uploadedfile);
                    //                        File.Delete(f.FileName);
                    //                        logger.Info("ImportExport Server - Import Lost Tag Success - File successfully archived . Filename: " + config.BakPath + uploadedfile);

                    //                    }
                    //                    else
                    //                    {
                    //                        System.IO.File.Copy(f.FileName, config.ErrorPath + uploadedfile);
                    //                        File.Delete(f.FileName);
                    //                        logger.Info("ImportExport Server - Import Lost Tag Failed. Please check logs for details - File successfully archived . Filename: " + config.ErrorPath + uploadedfile);
                    //                    }
                    //                }
                    //                catch (Exception ex)
                    //                {
                    //                    logger.Info("ImportExport Server - Import Lost Tags - xception Occured: Filename: " + f.FileName + ". " + ex.InnerException.ToString().ToString());
                    //                    SendNotification("ImportExport Server - Import Lost Tags", "Exception Occured: Filename: " + f.FileName + ". " + ex.InnerException.ToString().ToString());
                    //                }

                    //            }

                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            logger.Info("ImportExport Server - Import Lost Tags - Exception Occured: SFTP Import Lost Tags." + ex.InnerException.ToString().ToString());
                    //            SendNotification("ImportExport Server - Import Lost Tags", "Exception Occured: SFTP Import Lost Tags." + ex.InnerException.ToString().ToString());
                    //        }

                    //    }


                    //    //im.UpdateNextRunSchedule(a.Function.Trim());


                    //}

                    //else if (a.Function.Trim() == "Import Found Tag")
                    //{
                    //    ImportFile im = new ImportFile(SVADBConnStr);
                    //    if ((DateTime.Compare(DateTime.Now, a.ActiveTime) >= 0) && (a.ActTimes > 0))
                    //    {
                    //        try
                    //        {
                    //            im.UpdateNextRunSchedule(a.Function.Trim());
                    //            logger.Info("ImportExport Server - SFTP - Start SFTP Import Found Tags");
                    //            WinSCP w = new WinSCP();

                    //            w.GetLostTags();
                    //            logger.Info("ImportExport Server - SFTP - End SFTP Import Found Tags");

                    //            foreach (FileRecord f in GetFileList(config.ImportPath2))
                    //            {
                    //                try
                    //                {
                    //                    int result;
                    //                    result = im.ImportFoundTag(f.FileName);
                    //                    string uploadedfile = Path.GetFileName(f.FileName);

                    //                    if (result == ReturnCode.SUCCESSFUL)
                    //                    {

                    //                        System.IO.File.Copy(f.FileName, config.BakPath + uploadedfile);
                    //                        File.Delete(f.FileName);
                    //                        logger.Info("ImportExport Server - Import Found Tag Success - File successfully archived . Filename: " + config.BakPath + uploadedfile);

                    //                    }
                    //                    else
                    //                    {
                    //                        System.IO.File.Copy(f.FileName, config.ErrorPath + uploadedfile);
                    //                        File.Delete(f.FileName);
                    //                        logger.Info("ImportExport Server - Import Found Tag Failed. Please check logs for details - File successfully archived . Filename: " + config.ErrorPath + uploadedfile);
                    //                    }
                    //                }
                    //                catch (Exception ex)
                    //                {
                    //                    logger.Info("ImportExport Server - Import Found Tags - Exception Occured: Filename: " + f.FileName + ". " + ex.InnerException.ToString().ToString());
                    //                    SendNotification("ImportExport Server - Import Found Tags", "Exception Occured: Filename: " + f.FileName + ". " + ex.InnerException.ToString().ToString());
                    //                }

                    //            }

                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            logger.Info("ImportExport Server - Import Found Tags  - Exception Occured: SFTP Import Found Tags." + ex.InnerException.ToString().ToString());
                    //            SendNotification("ImportExport Server - Import Found Tags", "Exception Occured: SFTP Import Found Tags." + ex.InnerException.ToString().ToString());
                    //        }

                    //    }
                    //}
                    
                }
                
            }

        }




        private void ExportBaggage()
        {
            try
            {                
                ExportFile ex = new ExportFile(SVADBConnStr);
                int result;
                //ex.UpdateNextRunSchedule(a.Function.Trim());
                result = ex.ExportBaggagTags(config.ExportPath);

                if (result == ReturnCode.SUCCESSFUL)
                {
                    try
                    {
                        ex.SetCouponsToExported();
                        logger.Info("ImportExport Server - DoExport - Successful - Export Baggage Tags");


                        logger.Info("ImportExport Server - SFTP - Start SFTP Export Baggage Tag");
                        WinSCP w = new WinSCP();

                        w.TransferBaggageTag();
                        logger.Info("ImportExport Server - SFTP - End SFTP Export Baggage Tag");
                    }
                    catch (Exception e)
                    {
                        logger.Info("ImportExport Server - SFTP Export Baggage Tags - Exception Occured: SFTP Export Baggage Tags." + e.ToString());
                        SendNotification("ImportExport Server - SFTP Export Baggage Tags", "Exception Occured: SFTP Export Baggage Tags." + e.Message.ToString());
                    }
                }
                else
                {
                    logger.Info("ImportExport Server - DoExport - Failed - Export Baggage Tags. Please check log file for details. " + ex.ToString());
                }
            }
            catch (Exception e)
            {
                logger.Info("ImportExport Server - Export Baggage Tags - Exception Occured: Export Baggage Tags." + e.Message.ToString());
                SendNotification("ImportExport Server - Export Baggage Tags", "Exception Occured: Export Baggage Tags." + e.Message.ToString());
            }
        }

        private void ExportMapping()
        {
            try
            {

                ExportFile ex = new ExportFile(SVADBConnStr);
                int result;
                
                result = ex.GetMapping(config.ExportPath2);
                //ex.SetCouponsToExported();
                if (result == ReturnCode.SUCCESSFUL)
                {
                    try
                    {
                        ex.SetCouponsToExported();
                        logger.Info("ImportExport Server - DoExport - Successful - Export Mappings");

                        logger.Info("ImportExport Server - SFTP - Start SFTP Export Mappings");
                        WinSCP w = new WinSCP();

                        w.TransferMapping();
                        logger.Info("ImportExport Server - SFTP - End SFTP Export Mappings");
                    }
                    catch (Exception e)
                    {

                        logger.Info("ImportExport Server - SFTP Export Mappings - Exception Occured: SFTP Export Mappings." + e.Message.ToString());
                        SendNotification("ImportExport Server - SFTP Export Mappings", "Exception Occured: SFTP Export Mappings." + e.Message.ToString());
                    }
                }
                else
                {
                    logger.Info("ImportExport Server - DoExport - Failed - Export Mappings. Please check log file for details");
                }


            }
            catch (Exception e)
            {
                logger.Info("ImportExport Server - Export Mappings - Exception Occured: Export Mappings." + e.Message.ToString());
                SendNotification("ImportExport Server - Export Mappings", "Exception Occured: Export Mappings." + e.Message.ToString());
            }
        }

        private void ImportAirline()
        {
            ImportFile im = new ImportFile(SVADBConnStr);

            //logger.Info("ImportExport Server - SFTP", "Start SFTP Import Airline");
            logger.Info("ImportExport Server - SFTP  Start SFTP Import Airline");

            try
            {
                WinSCP w = new WinSCP();

                w.GetAirline();

                logger.Info("ImportExport Server - SFTP - End SFTP Import Airline");
            }
            catch (Exception ex)
            {
                logger.Info("ImportExport Server - Import Airline - Exception Occured: SFTP Import Airline." + ex.InnerException.ToString().ToString());
                SendNotification("ImportExport Server - Import Airline", "Exception Occured: SFTP Import Airline." + ex.InnerException.ToString().ToString());

            }

            foreach (FileRecord f in GetFileList(config.ImportPath))
            {
                try
                {
                    int result;
                    logger.Info("ImportExport Server - Import Airline - Start" + DateTime.Now.ToString() + " Import Airlines");

                    result = im.ImportAirline(f.FileName);
                    string uploadedfile = Path.GetFileName(f.FileName);

                    if (result == ReturnCode.SUCCESSFUL)
                    {

                        System.IO.File.Copy(f.FileName, config.BakPath + uploadedfile);
                        File.Delete(f.FileName);
                        logger.Info("ImportExport Server - Import Airline Success - File successfully archived . Filename: " + config.BakPath + uploadedfile);

                    }
                    else
                    {
                        System.IO.File.Copy(f.FileName, config.ErrorPath + uploadedfile);
                        File.Delete(f.FileName);
                        logger.Info("ImportExport Server - Import Airline Failed. Please check logs for details - File successfully archived . Filename: " + config.ErrorPath + uploadedfile);
                        SendNotification("ImportExport Server - Import Airline Failed. Please check logs for details", "File successfully archived . Filename: " + config.ErrorPath + uploadedfile);
                    }
                }
                catch (Exception ex)
                {
                    logger.Info("ImportExport Server - Import Airline - Exception Occured: Filename: " + f.FileName + ". " + ex.InnerException.ToString().ToString());
                    SendNotification("ImportExport Server - Import Airline", "Exception Occured: Filename: " + f.FileName + ". " + ex.InnerException.ToString().ToString());
                }
            }                            
        }

        private void ImportLostTag()
        {
            ImportFile im = new ImportFile(SVADBConnStr);
            try
            {
                logger.Info("ImportExport Server - SFTP - Start SFTP Import Lost Tags");
                WinSCP w = new WinSCP();

                w.GetLostTags();
                logger.Info("ImportExport Server - SFTP - End SFTP Import Lost Tags");
            }
            catch (Exception ex)
            {
                logger.Info("ImportExport Server - SFTP Import Lost Tags - Exception Occured: SFTP Import Lost Tags " + ex.InnerException.ToString().ToString());
                SendNotification("ImportExport Server - SFTP Import Lost Tags", "Exception Occured: Import Lost Tags " + ex.InnerException.ToString().ToString());
            }

            foreach (FileRecord f in GetFileList(config.ImportPath2))
            {
                logger.Info("ImportExport Server - Import Lost Tag - Start" + DateTime.Now.ToString() + " Import Lost Tag");

                try
                {
                    int result;
                    result = im.ImportLostTag(f.FileName);
                    string uploadedfile = Path.GetFileName(f.FileName);

                    if (result == ReturnCode.SUCCESSFUL)
                    {

                        System.IO.File.Copy(f.FileName, config.BakPath + uploadedfile);
                        File.Delete(f.FileName);
                        logger.Info("ImportExport Server - Import Lost Tag Success - File successfully archived . Filename: " + config.BakPath + uploadedfile);

                    }
                    else
                    {
                        System.IO.File.Copy(f.FileName, config.ErrorPath + uploadedfile);
                        File.Delete(f.FileName);
                        logger.Info("ImportExport Server - Import Lost Tag Failed. Please check logs for details - File successfully archived . Filename: " + config.ErrorPath + uploadedfile);
                        SendNotification("ImportExport Server - Import Lost Tag Failed. Please check logs for details", "File successfully archived . Filename: " + config.ErrorPath + uploadedfile);
                    }
                }
                catch (Exception ex)
                {
                    logger.Info("ImportExport Server - Import Lost Tag - Exception Occured: Filename: " + f.FileName + ". " + ex.InnerException.ToString().ToString());
                    SendNotification("ImportExport Server - Import Lost Tag", "Exception Occured: Filename: " + f.FileName + ". " + ex.InnerException.ToString().ToString());
                }
            }
        }


        private void ImportFoundTag()
        {
            ImportFile im = new ImportFile(SVADBConnStr);
            try
            {
                logger.Info("ImportExport Server - SFTP - Start SFTP Import Found Tags");
                WinSCP w = new WinSCP();

                w.GetFoundTags();
                logger.Info("ImportExport Server - SFTP - End SFTP Import Found Tags");
            }
            catch (Exception ex)
            {
                logger.Info("ImportExport Server - SFTP Import Found Tags - Exception Occured: SFTP Import Found Tags " + ex.InnerException.ToString().ToString());
                SendNotification("ImportExport Server - SFTP Import Found Tags", "Exception Occured: Import Found Tags " + ex.InnerException.ToString().ToString());
            }

            foreach (FileRecord f in GetFileList(config.ImportPath2))
            {
                logger.Info("ImportExport Server - Import Found Tag - Start" + DateTime.Now.ToString() + " Import Found Tag");

                try
                {
                    int result;
                    result = im.ImportFoundTag(f.FileName);
                    string uploadedfile = Path.GetFileName(f.FileName);

                    if (result == ReturnCode.SUCCESSFUL)
                    {

                        System.IO.File.Copy(f.FileName, config.BakPath + uploadedfile);
                        File.Delete(f.FileName);
                        logger.Info("ImportExport Server - Import Found Tag Success - File successfully archived . Filename: " + config.BakPath + uploadedfile);

                    }
                    else
                    {
                        System.IO.File.Copy(f.FileName, config.ErrorPath + uploadedfile);
                        File.Delete(f.FileName);
                        logger.Info("ImportExport Server - Import Found Tag Failed. Please check logs for details - File successfully archived . Filename: " + config.ErrorPath + uploadedfile);
                        SendNotification("ImportExport Server - Import Found Tag Failed. Please check logs for details", "File successfully archived . Filename: " + config.ErrorPath + uploadedfile);
                    }
                }
                catch (Exception ex)
                {
                    logger.Info("ImportExport Server - Import Found Tag - Exception Occured: Filename: " + f.FileName + ". " + ex.InnerException.ToString().ToString());
                    SendNotification("ImportExport Server - Import Found Tag", "Exception Occured: Filename: " + f.FileName + ". " + ex.InnerException.ToString().ToString());
                }
            }
        }

        private void GetActiveTimeList()
        {
            try
            {
                ActiveTimeList.Clear();

                config = AppConfig.GetSingleton();
                SVADBConnStr = config.SVAConnectionString;
                ExportFile ex = new ExportFile(SVADBConnStr);

                madt = ex.GetImportExportMonitoringRulee();
                
                foreach (DataRow dr in madt.Rows)
                {
                    ActiveRecord a = new ActiveRecord();
                    a.Function = dr["Func"].ToString();
                    a.DownloadPath = dr["DownloadPath"].ToString();
                    a.ActiveTime = DateTime.Parse(dr["ActiveTime"].ToString());
                    a.ActTimes = 1;

                    ActiveTimeList.Add(a);
                }
            }
            catch (Exception ex)
            {                
                SendNotification("Error in GetActiveTimeList", ex.InnerException.ToString());
                throw ex;
            }
        }

        private void GetLastActiveTime()
        {
            config = AppConfig.GetSingleton();
            SVADBConnStr = config.SVAConnectionString;
            ExportFile ex = new ExportFile(SVADBConnStr);
    
            DateTime lastActiveTime = ex.GetLastActiveTime();

            TimeSpan varTime = DateTime.Now - lastActiveTime;

            if (varTime.TotalSeconds > 60)
            {

                ex.UpdateNextRunSchedule("Import and Export Files Exchange");
            }
        }
  
        private List<FileRecord> GetFileList(string PathName)
        {
            List<FileRecord> result = new List<FileRecord>();
      
            if (!IsDirectoryEmpty(PathName))
            {
                logger.Info("ImportExport Server - GetFileList - GetFileList PathName:" + PathName);
            }
            
            string filepath = PathName;
            if (!IsDirectoryEmpty(filepath))
            {
                logger.Info("ImportExport Server-  GetFileList - filepath :" + filepath);
            }

            try
            {
                if (Directory.Exists(filepath))
                {
                    logger.Info("ImportExport Server - GetFileList - Directory.Exists :" + filepath);
                    try
                    {
                        string[] fileDirs = Directory.GetDirectories(filepath);
                        if (fileDirs.Count() != 0)
                        {
                            logger.Info("ImportExport Server - GetFileList - fileDirs.Count() :" + fileDirs[0]);
                        }
                        else
                        {
                            logger.Info("ImportExport Server - GetFileList - fileDirs.Count() :" + fileDirs.Count());
                        }

                        string Dirname = PathName;
                        if (!IsDirectoryEmpty(Dirname))
                        {
                            logger.Info("ImportExport Server - GetFileList - Dirname :" + Dirname);
                            int lastp = Dirname.LastIndexOf("\\");
                            string temp = Dirname.Substring(lastp + 1, Dirname.Length - lastp - 1);
                            logger.Info("ImportExport Server - GetFileList - Transaction Number :" + temp);
                            string[] filenames = Directory.GetFiles(Dirname);
                            foreach (string fname in filenames)
                            {
                                if (File.Exists(fname))
                                {
                                    FileRecord FF = new FileRecord();
                                    FF.OrderNumber = temp;
                                    FF.FileName = fname;
                                    result.Add(FF);
                                }
                            }
                        }
                        else
                        {
                            logger.Info("ImportExport Server - GetFileList - Dirname :" + Dirname + " is Empty");
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Info("ImportExport Server - Error Occured - GetFileList:" + ex.InnerException.ToString());
                        SendNotification("ImportExport Server - Error Occured", "GetFileList:" + ex.InnerException.ToString());
                    }

                }
            }
            catch (System.Exception ex)
            {
                logger.Info("ImportExport Server - Error Occured - GetFileList:" + ex.InnerException.ToString());
                SendNotification("ImportExport Server - Error Occured", "GetFileList:" + ex.InnerException.ToString());
            }
            return result;
        }

        public bool IsDirectoryEmpty(string path)
        {
            return IsDirectoryEmpty(new DirectoryInfo(path));
        }

        public bool IsDirectoryEmpty(DirectoryInfo directory)
        {
            FileInfo[] files = directory.GetFiles();
            DirectoryInfo[] subdirs = directory.GetDirectories();

            return (files.Length == 0 && subdirs.Length == 0);
        }

        private void SendNotification(string failedtask, string message)
        {

            AppFunctions af = new AppFunctions();
            config = AppConfig.GetSingleton();
            string sendfrom = config.SendFrom;
            string receipient = config.EmailReceipients;
            string password = config.EmailPassword;
            string smtphost = config.SMTPHost;
            string smtpport = config.SMTPPort;

            string subject = "HKIA TMS Windows Service Notification";
            string body = "Dear HKIA TMS admin, " +
                Environment.NewLine +
                Environment.NewLine +
                "This is to notify you of activity or error in HKIA TMS Windows Service with details below" +
                Environment.NewLine +
                Environment.NewLine +
                failedtask + ", " + message +
                Environment.NewLine +
                Environment.NewLine +
                "";
            try
            {
                af.SendEmail(subject, body, sendfrom, receipient, password, smtphost, smtpport);
            }
            catch (Exception ex)
            {
                logger.Info("SMTP Mail: " + ex.InnerException.ToString());
            }
        }
      
    }
}
