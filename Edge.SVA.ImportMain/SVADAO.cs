﻿using System;
using System.Data;
using Edge.SVA.ImportMain.Constants;
using System.Diagnostics;
using log4net;

namespace Edge.SVA.ImportMain
{
    public class SVADAO
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        DatabaseUtil.Interface.IDatabase database;
        public SVADAO(string connstr)
        {
            DatabaseUtil.Factory.SetConnecctionString(connstr);
            DatabaseUtil.Factory.SetDefaultTimeout(600);
            database = DatabaseUtil.Factory.CreateIDatabase();
        }

        public void InsertAirline(string AirlineCode)
        {
            string queryString = "Insert into EligibleAirlineList " +
                                    "( " +
                                        "EligibleAirlineCode " +
                                    ") " +
                                    "Values " +
                                    "( " +
                                       " '" + AirlineCode + "' " +
                                    ") ";

            try
            {
                database.ExecuteSql(queryString);
            }
            catch (Exception ex)
            {
                logger.Info("Excption occured: Method name: InsertAirline ", ex);
            }
        }

        public DataTable GetBaggageTags()
        {
            
            string queryString = "SELECT c.CouponNumber as BaggageTag, " +
                                    "CASE WHEN c.Status = 2 then 'A' " +
                                    "WHEN c.Status = 5 then 'D' " +  //void
                                    "WHEN c.Status = 4 then 'L' " + //lost / expired
                                    "WHEN c.Status = 0 AND c.StockStatus = 8 THEN 'D' " +
                                    "WHEN c.Status = 0 AND c.StockStatus = 7 THEN 'D' " +
                                    "End as Action " +                                    
                                    "From Coupon c " +
                                    "Where ((c.Status IN (2, 4, 5)) " +
                                     "OR (c.Status = 0 AND c.StockStatus in( 7, 8)) " +
                                     "OR (c.Status = 2 AND c.StockStatus = 8)) " +
                                    "And Exported = 0";
            try
            {
                DataTable dt = database.Query(queryString, "dt");

                return dt;
            }
            catch (Exception ex)
            {
                logger.Info("Excption occured: Method name: GetBaggageTags ", ex);
                throw ex;
            }
        }
       
        public DataTable GetMapping()
        {

            string queryString = "SELECT c.QRCodePrefix as BaggageTagPrefix, c.BrandCode as Airline from CouponType c Where c.BrandCode <> '00' ";

            try
            {
                DataTable dt = database.Query(queryString, "dt");



                //insert this into the datatable
                DataRow newRow = dt.NewRow();
                newRow["BaggageTagPrefix"] = "00";
                newRow["Airline"] = "*";
                dt.Rows.Add(newRow);
                ////sort datatable asc so inserted row will be first
                DataView dv = dt.DefaultView;
                dv.Sort = "BaggageTagPrefix ASC";
                dt = dv.ToTable();

                return dt;
            }
            catch (Exception ex)
            {
                logger.Info("Excption occured: Method name: GetMapping ", ex);
                throw ex;
            }

        }

        public DataTable GetImportExportMonitoringRule()
        {
            string queryString = " select RuleName, Description, Func, DownloadPath, ActiveTime from ImporExportMonitoringRule H "
               + " where  ( (isnull(H.DayFlagID, 0) in (0,-1)) or ( (isnull(H.DayFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.DayFlagID, 1, getdate(), 1) = 1) ) ) "
               + " and ( (isnull(H.WeekFlagID, 0) in (0,-1)) or ( (isnull(H.WeekFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.WeekFlagID, 2, getdate(), 1) = 1) ) ) "
               + " and ( (isnull(H.MonthFlagID, 0) in (0,-1)) or ( (isnull(H.MonthFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.MonthFlagID, 3, getdate(), 1) = 1) ) ) "
               + " and Status = 1 and DATEDIFF(dd, StartDate, getdate()) > 0 and DATEDIFF(dd, EndDate, getdate()) < 0 "
               + " and left(cast (H.ActiveTime as time),5) = left(cast(CONVERT (time, GETDATE()) as CHAR) , 5)";
            try
            {
                DataTable dt = database.Query(queryString, "dt");


                return dt;
            }
            catch (Exception ex)
            {
                logger.Info("Excption occured: Method name: GetImportExportMonitoringRule ", ex);
                throw ex;
            }
        }

        public void SetCouponsToExported()
        {
            string queryString = "Update Coupon " +
                        "Set Exported =  1 " +
                         "Where Exported = 0" ;

            try
            {
                database.ExecuteSql(queryString);
            }
            catch (Exception ex)
            {
                logger.Info("Excption occured: Method name: SetCouponsToExprted ", ex);
            }
        }

        public void SetCouponsToLost(string coupounnumber, DateTime datetimeupdated)
        {
            string queryString = "Update Coupon " +
                        "Set Status =  4, " +
                        "UpdatedOn = '" + datetimeupdated + "' " +
                         "Where CouponNumber = '" + coupounnumber+ "' " +
                         "And Status = 2 ";
            try
            {
                database.ExecuteSql(queryString);
            }
            catch (Exception ex)
            {
                logger.Info("Excption occured: Method name: SetCouponsToLost " ,ex);
            }
        }

        public void SetCouponsToIssuedInLocation(DateTime datetimeupdated)
        {
            string queryString = "Update Coupon " +
                        "Set Status =  2, " +
                        "StockStatus = 6, " +
                        "UpdatedOn = '" + datetimeupdated + "' " +
                         "Where Status = 4 ";
            try
            {
                database.ExecuteSql(queryString);
            }
            catch (Exception ex)
            {
                logger.Info("Excption occured: Method name: SetCouponsToLost ", ex);
            }
        }

        public void SetCouponsToIssuedInLocation(string coupounnumber, DateTime datetimeupdated)
        {
            string queryString = "Update Coupon " +
                        "Set Status =  2, " +
                        "StockStatus = 6, " +
                        "UpdatedOn = '" + datetimeupdated + "' " +
                         "Where CouponNumber = '" + coupounnumber + "' " +
                         "And Status = 4 ";
            try
            {
                database.ExecuteSql(queryString);
            }
            catch (Exception ex)
            {
                logger.Info("Excption occured: Method name: SetCouponsToLost ", ex);
            }
        }

        public DataTable GetListofLostTags()
        {
            string queryString = "Select c.CouponNumber form Coupon c. Where Status = 4 ";
            DataTable dt = database.Query(queryString, "dt");

            return dt;
        }

        public void UpdateNextRunSchedule(string Func)
        {
            int FileExchangeIntervalMin = GetFileExchangeInterval();

            string queryString = "Update ImporExportMonitoringRule " +
                        "Set ActiveTime =  DATEADD(MINUTE, " + FileExchangeIntervalMin + ", CAST(GETDATE() AS SMALLDATETIME)) " +
                         "Where Func = '" + Func + "'";

            try
            {
                database.ExecuteSql(queryString);
                DateTime nextruntime = GetLastActiveTime().AddMinutes(FileExchangeIntervalMin);
                logger.Info("UpdateNextRunSchedule: Next Runtime: " + nextruntime.ToString() );
            }
            catch (Exception ex)
            {
                logger.Info("UpdateNextRunSchedule ", ex);
            }
        }

        private int GetFileExchangeInterval()
        {
            string queryString = "SELECT f.FileExchangeIntervalMin from FileExchangeSetting f Where f.ID = 1";
            DataTable dt = database.Query(queryString, "dt");

            return int.Parse(dt.Rows[0]["FileExchangeIntervalMin"].ToString());
        }

        public DateTime GetLastActiveTime()
        {
            string queryString = "SELECT [ActiveTime] FROM [dbo].[ImporExportMonitoringRule] WHERE [RuleName] = '05'";
            DataTable dt = database.Query(queryString, "dt");

            return DateTime.Parse(dt.Rows[0]["ActiveTime"].ToString());
        }


    }


    

}
