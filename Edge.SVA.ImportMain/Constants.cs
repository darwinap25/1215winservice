﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Edge.SVA.ImportMain.Constants
{
  
        [Serializable()]
        public class Exists
        {
            public const int YES = 1;
            public const int NO = 0;
        }

        [Serializable()]
        public class Confirm
        {
            public const int intYES = 1;
            public const int intNO = 0;
            public const string strYES = "1";
            public const string strNO = "0";
            public const bool boolYES = true;
            public const bool boolNO = false;
        }

        [Serializable()]
        public class ReturnCode 
        {
            public const int SUCCESSFUL = 0;
            public const int EXCEPTION_ENCOUNTERED = -1;
            public const int PPROCESS_FAILED = 2;
        }

        [Serializable()]
        public class RRC_Type_of_Application 
        {
            public const int NEW = 1;
            public const int RENEWAL = 2;
            public const int REPLACEMENT = 3;
            public const int EXPIRED = 4;
            public const int UNKNOWN_RRC_TYPE_OF_APLICATION = 5;
        }

        [Serializable()]
        public class MemberInternalKey 
        {

        }

        [Serializable()]
        public class ApplicationAction
        {
            public const string IMPORT = "I";
            public const string EXPORT = "E";
        }
        
        [Serializable()]
        public class StatusCode
        {
            public const string ACTIVE = "1";
            public const string INACTIVE = "0";
        }

        [Serializable()]
        public class SegmentStatus
        {
            public const string ATTACH = "A";
            public const string DETACH = "D";

            public const string numATTACH = "1";
            public const string numDETACH = "2";
        }

        [Serializable()]
        public class ApproveStatus
        {
            public const string PENDING = "P";
            public const string APPROVED = "A";
            public const string VOID = "V";
        }

        [Serializable()]
        public class ImportStatus
        {
            public const string PENDING = "P";
            public const string SUCCESS = "S";
            public const string FAILED = "F";
        }

        [Serializable()]
        public class CardStatus
        {
            public const string DORMANT = "0";
            public const string PENDING = "1";
            public const string ISSUED = "2";
            public const string REDEEMED = "3";
            public const string VOID = "4";
        }

        [Serializable()]
        public class MaritalStatus
        {
            public const string SINGLE = "1";
            public const string MARRIED = "2";
        }

        [Serializable()]
        public class CustomerTier
        {
            public const string CLASSIC = "1";
            public const string ELITE = "2";
            public const string BEEP = "3";
        }

        [Serializable()]
        public class BusinessOwner
        {
            public const string YES = "1";
            public const string NO = "2";
        }

        [Serializable()]
        public class EmailSubscription
        {
            public const string YES = "1";
            public const string NO = "2";
        }

        [Serializable()]
        public class SMSSubscription
        {
            public const string YES = "1";
            public const string NO = "2";
        }

        [Serializable()]
        public class Gender
        {
            public const string MALE = "1";
            public const string FEMALE = "2";
            public const string TESTCARD = "9";
        }

        [Serializable()]
        public class MessageAlert
        {
            public const string HOUSEHOLD_EXTERNALID_NOT_FOUND = "not find HouseHoldExternalId";
            public const string MEMBER_EXTERNALID_NOT_FOUND = "not find MemberExternalId";
            public const string TWO_CARD_ALL_NOT_FOUND = "2 card all not find record";
            public const string GET_MEMBER_INTERNALKEY_NOT_FOUND = "GetMemberInternalKey not find record";
            public const string GET_BUYING_UNIT_INTERNALKEY_NOT_FOUND = "GetBuyingUnitInternalKey not find record";
            public const string GET_BUYING_UNIT_INTERNALKEY_NOT_FIND_MEMBER_INTERNALKEY = "GetBuyingUnitInternalKey not find MemberInternalKey";
            public const string GET_EXTERNAL_BUYING_UNIT_NOT_FOUND = "GetExternalBuyingUnit not find record";
            public const string GET_BUYING_UNIT_INTERNALKEY_NOT_FIND_BUYING_UNIT_INTERNALKEY1 = "GetBuyingUnitInternalKey not find BuyingUnitInternalKey1";
            public const string RRC_CUSTOMER_TIER_IS_EMPTY = "CSV Column RRC_Customer Tier is empty";
            public const string RRC_BUSINESS_OWNER_IS_EMPTY = "CSV Column RRC_Business Owner is empty";
            public const string RRC_EMAIL_SUBSCRIPTION_IS_EMPTY = "CSV Column RRC_Email Subscription is empty";
            public const string RRC_SMS_SUBSCRIPTION_IS_EMPTY = "CSV Column RRC_SMS Subscription is empty";
            
        }
}
