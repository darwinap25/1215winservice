﻿using System;
using System.Data;
using Edge.SVA.ImportMain.Constants;

namespace Edge.SVA.ImportMain
{
    public class LoyaltyDAO
    {
        DatabaseUtil.Interface.IDatabase database;
        public LoyaltyDAO(string connstr)
        {
            DatabaseUtil.Factory.SetConnecctionString(connstr);
            DatabaseUtil.Factory.SetDefaultTimeout(600);
            database = DatabaseUtil.Factory.CreateIDatabase();
        }

        public DataTable GetMemberDetailsAddedOrLinked(string RRC_Old_CardNo)
        {

            string queryString = "SELECT c.MemberInternalKey, c.ClubCardId, c.RestrictionId, c.ExpirationDate, " +
                                    "m.ExternalMemberKey, m.BuyingUnitInternalKey," +
                                    " b.ExternalBuyingUnit,  b.PostalCode, b.Country, b.Street1, b.City, b.HomePhone, b.EmailAddress, b.SendEmail," +
                                    " m2.ExternalMemberKey as OldestExternalMemberKey, m2.BirthDate, m2.MobilePhoneNumber, m2.WorkPhoneNumber, m2.Gender" +
                                    " FROM dbo.CRM_Clubcard c" +
                                    " INNER JOIN dbo.CRM_Member m ON c.MemberInternalKey = m.MemberInternalKey" +
                                    " INNER JOIN dbo.CRM_BuyingUnit b ON m.BuyingUnitInternalKey = b.BuyingUnitInternalKey" +
                                    " INNER JOIN dbo.CRM_Member m2 ON m2.BuyingUnitInternalKey = m.BuyingUnitInternalKey" +
                                    " WHERE c.ClubInternalKey = 2 AND c.ClubCardId  = '" + RRC_Old_CardNo + "'" +
                                    " AND m2.IsMainMember = 1";

            return database.Query(queryString, "dt");

        }

        public string GetExternalMemberKeyByRRC_Old_CardNo(string RRC_Old_CardNo)
        {

            string queryString = "SELECT m.ExternalMemberKey " +
                                    " FROM dbo.CRM_Clubcard c" +
                                    " INNER JOIN dbo.CRM_Member m ON c.MemberInternalKey = m.MemberInternalKey" +
                                    " INNER JOIN dbo.CRM_BuyingUnit b ON m.BuyingUnitInternalKey = b.BuyingUnitInternalKey" +
                                    " INNER JOIN dbo.CRM_Member m2 ON m2.BuyingUnitInternalKey = m.BuyingUnitInternalKey" +
                                    " WHERE c.ClubInternalKey = 2 AND c.ClubCardId  = '" + RRC_Old_CardNo + "'" +
                                    " AND m2.IsMainMember = 1";

            DataTable dt = database.Query(queryString, "dt");

            dt = database.Query(queryString, "dt");

            return dt.Rows[0]["ExternalMemberKey"].ToString(); 

        }

        public string GetExternalMemberKeyByBuyingUnitInternalKey(string BuyingUnitInternalKey)
        {
            string queryString = "Select m.ExternalMemberKey " +
                                    "From dbo.CRM_BuyingUnit b " +
                                    "Inner Join dbo.CRM_Member m " +
                                        "On m.BuyingUnitInternalKey = b.BuyingUnitInternalKey " +
                                    "Where b.BuyingUnitInternalKey = '" + BuyingUnitInternalKey + "' " +
                                        "and m.IsMainMember = 1";

            DataTable dt = database.Query(queryString, "dt");

            dt = database.Query(queryString, "dt");

            return dt.Rows[0]["ExternalMemberKey"].ToString();
        }


        public DataTable GetMemberDetails1(string CardNumber)
        {

            string queryString = "Select ClubCardId, MemberInternalKey, ExpirationDate from dbo.CRM_Clubcard where ClubCardId = '" + CardNumber + "'";

            DataTable dt = database.Query(queryString, "dt");

            return dt;
            
        }

        public DataTable GetMemberDetails1(string CardNumber, string OldCardNumber, int IsExists)
        {
            string queryString = "";

            if (IsExists == Exists.NO)
            {
                queryString = "Select c.ClubCardId, c.MemberInternalKey, c.ExpirationDate, m.ExternalMemberKey, m.BuyingUnitInternalKey " +
                                "From dbo.CRM_Clubcard c " +
                                "Inner Join dbo.CRM_Member m " +
                                "On c.MemberInternalKey = m.MemberInternalKey " +
                                "Where ClubCardId = '" + OldCardNumber + "' and ClubInternalKey = 2";
            }
            else 
            {
                queryString = "Select c.ClubCardId, c.MemberInternalKey, c.ExpirationDate, m.ExternalMemberKey, m.BuyingUnitInternalKey " +
                                "From dbo.CRM_Clubcard c " +
                                "Inner Join dbo.CRM_Member m " +
                                "On c.MemberInternalKey = m.MemberInternalKey " +
                                "Where ClubCardId = '" + CardNumber + "' and ClubInternalKey = 2";
            }

            DataTable dt = database.Query(queryString, "dt");
            
            return dt;
        }


        //same query whether card already exists or not
        public string GetBuyingUnitInternalKey(string MemberInternalKey, int IsMainMember) 
        {

            string queryString = "Select BuyingUnitInternalKey from dbo.CRM_Member where MemberInternalKey= '" + MemberInternalKey + "' and IsMainMember =  '" + IsMainMember + "'";

            DataTable dt = database.Query(queryString, "dt");

            return dt.Rows[0]["BuyingUnitInternalKey"].ToString();
        }

        //same query whether card already exists or not
        public string GetExternalBuyingUnit(string BuyingUnitInternalKey)
        {
            string queryString = "Select ExternalBuyingUnit from dbo.CRM_BuyingUnit where BuyingUnitInternalKey= '" + BuyingUnitInternalKey + "'";

            DataTable dt = database.Query(queryString, "dt");

            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["ExternalBuyingUnit"].ToString();
            }
            else
            {
                return "";
            }
        }

        public DateTime GetCardExpirationDate(string CardNumber)
        {
            string queryString = "Select ExpirationDate from dbo.CRM_Clubcard where ClubCardId = '" + CardNumber + "'";
            DataTable dt = database.Query(queryString, "dt");
            
            return DateTime.Parse(dt.Rows[0]["ExpirationDate"].ToString()).AddYears(2);            
        }

        public string GetOldCardBalance(string BuyingUnitInternalKey, string RRC_Loyalty_Card)
        {
            string queryString = "Select Balance from dbo.CRM_BuyingUnitAccountsActivity where BuyingUnitInternalKey= '" + BuyingUnitInternalKey + "'";// and AccountInternalKey = '" + RRC_Loyalty_Card + "'";
            DataTable dt = database.Query(queryString, "dt");

            return dt.Rows[0]["Balance"].ToString();
        }

        public DataTable GetMemberDetails(string CardNumber, int rrctype)
        {
            string queryString;

            if (rrctype == RRC_Type_of_Application.NEW)
            {
                queryString = "SELECT TOP (1) m.MemberInternalKey, c.ClubCardId, c.RestrictionId, m.ExternalMemberKey, m.BuyingUnitInternalKey, c.ExpirationDate, b.ExternalBuyingUnit from dbo.CRM_Clubcard c"
                                + "INNER JOIN dbo.CRM_Member m ON c.MemberInternalKey = m.MemberInternalKey "
                                + "INNER JOIN dbo.CRM_BuyingUnit b ON m.BuyingUnitInternalKey = b.BuyingUnitInternalKey"
                                + "WHERE c.ClubCardId = " + CardNumber + " and IsMainMember=1 order by BuyingUnitInternalKey desc";
            }
            else
            {
                queryString = "SELECT TOP (1) m.MemberInternalKey, c.ClubCardId, c.RestrictionId, m.ExternalMemberKey, m.BuyingUnitInternalKey, b.ExternalBuyingUnit from dbo.CRM_Clubcard c"
                                + "INNER JOIN dbo.CRM_Member m ON c.MemberInternalKey = m.MemberInternalKey "
                                + "INNER JOIN dbo.CRM_BuyingUnit b ON m.BuyingUnitInternalKey = b.BuyingUnitInternalKey"
                                + "WHERE c.ClubCardId = " + CardNumber + " order by BuyingUnitInternalKey desc";
            }

            DataTable dt = database.Query(queryString, "dt");

            return dt;
        }

        public DataTable GetOldMemberDetails(string BuyingUnitInternalKey)
        {
            string queryString = "Select ExternalMemberKey, MemberInternalKey, BuyingUnitInternalKey, BirthDate, MobilePhoneNumber, WorkPhoneNumber, Gender from dbo.CRM_Member where IsMainMember= 1 and BuyingUnitInternalKey='" + BuyingUnitInternalKey + "'";
            DataTable dt = database.Query(queryString, "dt");

            return dt;
        }

        public DataTable GetHouseholdDetails(string CardNumber, string MemberInternalKey, int rrctype, int IsMainMember)
        {
            string BuyingUnitInternalKey;

            BuyingUnitInternalKey = this.GetBuyingUnitInternalKey(MemberInternalKey, IsMainMember);

            string queryString = "Select ExternalBuyingUnit, BuyingUnitInternalKey, PostalCode, Country, Street1, City, HomePhone, EMailAddress, SendEmail " +
                                    "from dbo.CRM_BuyingUnit where BuyingUnitInternalKey='" + BuyingUnitInternalKey + "'";
            DataTable dt = database.Query(queryString, "dt");

            return dt;
        }

        public DataTable GetMemberAttributeValues(string MemberInternalKey)
        {

            string queryString = "Select AttributeId, StringValue, LongValue from dbo.CRM_MemberAttributeValue where MemberInternalKey='" + MemberInternalKey + "'";
            DataTable dt = database.Query(queryString, "dt");

            return dt;
        }

   

      

    }


}
