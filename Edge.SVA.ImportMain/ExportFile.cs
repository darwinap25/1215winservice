﻿using Common;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Edge.SVA.ImportMain.Constants;

namespace Edge.SVA.ImportMain
{
    public class ExportFile
    {
        private ILog logger = LogManager.GetLogger("ImportExportFile");
        string SVADBConnStr = "";
        AppConfig config;

        public ExportFile(string SVADBStr)
        {
            config = AppConfig.GetSingleton();            
            SVADBConnStr = SVADBStr;
            
        }

        public int ExportBaggagTags(string exportpath)
        {
            int fnReturnValue;
            DateTime date = DateTime.Now;
            string filedate = date.ToString("yyyyMMddHHmmsssss");

            string filename = exportpath + "baggagetag_" + filedate + ".csv";
            string filename2 = exportpath + "baggagetag_" + filedate + ".ok";
            string result = "";

            logger.Info("");
            logger.Info("----------------- New Export Baggage Tags -------------");
            logger.Info("-------------------------------------------------------");
            logger.Info("-------------------------------------------------------");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            logger.Info("Time Started: " + DateTime.Now.ToString());
            logger.Info("FullPath:" + filename);
            logger.Info("ExportPath:" + config.ExportPath);

            try
            {
                SVADAO dao = new SVADAO(SVADBConnStr);
                DataTable dt = dao.GetBaggageTags();

                if (dt.Rows.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();

                    string[] columnNames = dt.Columns.Cast<DataColumn>().
                                                      Select(column => column.ColumnName).
                                                      ToArray();
                    sb.AppendLine(string.Join(",", columnNames));

                    foreach (DataRow row in dt.Rows)
                    {
                        string[] fields = row.ItemArray.Select(field => field.ToString()).
                                                        ToArray();
                        sb.AppendLine(string.Join(",", fields));
                    }


                    File.WriteAllText(filename, sb.ToString());
                    File.WriteAllText(filename2, sb.ToString());
                    result = "EXPORT BAGGAGE TAG SUCCESS";
                    fnReturnValue = ReturnCode.SUCCESSFUL;
                }
                else
                {
                    result = "NO RECORDS TO EXPORT";
                    fnReturnValue = ReturnCode.SUCCESSFUL;
                }
                

            }
            catch (Exception ex)
            {
                logger.Info("Error Occured. Method name ExportBaggagTags. " + ex.InnerException.ToString());
                result = "EXPORT BAGGAGE TAG FAILED";
                fnReturnValue = ReturnCode.EXCEPTION_ENCOUNTERED;
                SendNotification("Error Occured. Method name ExportBaggagTags. " , ex.InnerException.ToString());
            }
            stopwatch.Stop();
            var elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
            logger.Info("Time Finsihed: " + DateTime.Now.ToString());
            logger.Info("Total processing time: " + elapsed_time + " seconds");
            logger.Info(result);
            logger.Info("-------------------------------------------------------");
            logger.Info("-------------------------------------------------------");
            logger.Info("----------------------- End File-----------------------");
            logger.Info("");

            return fnReturnValue;
        }

        public int GetMapping(string exportpath)
        {
            int fnReturnValue;
            DateTime date = DateTime.Now;
            string filedate = date.ToString("yyyyMMddHHmmsssss");

            string filename = exportpath + "mapping_" + filedate + ".csv";
            string filename2 = exportpath + "mapping_" + filedate + ".ok";
            
            string result = "";

            logger.Info("");
            logger.Info("------------------ New Export Mapping -----------------");
            logger.Info("-------------------------------------------------------");
            logger.Info("-------------------------------------------------------");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            logger.Info("Time Started: " + DateTime.Now.ToString());
            logger.Info("FullPath:" + filename);
            logger.Info("ExportPath:" + config.ExportPath2);

            try
            {
                SVADAO dao = new SVADAO(SVADBConnStr);
                DataTable dt = dao.GetMapping();

                if (dt.Rows.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();

                    string[] columnNames = dt.Columns.Cast<DataColumn>().
                                                      Select(column => column.ColumnName).
                                                      ToArray();
                    sb.AppendLine(string.Join(",", columnNames));

                    foreach (DataRow row in dt.Rows)
                    {
                        string[] fields = row.ItemArray.Select(field => field.ToString()).
                                                        ToArray();
                        sb.AppendLine(string.Join(",", fields));
                    }

                    File.WriteAllText(filename, sb.ToString());
                    File.WriteAllText(filename2, sb.ToString());
                    result = "EXPORT MAPPING SUCCESS";
                    fnReturnValue = ReturnCode.SUCCESSFUL;
                }
                else
                {
                    result = "NO RECORDS TO EXPORT";
                    fnReturnValue = ReturnCode.SUCCESSFUL;
                }
                
            }
            catch (Exception ex)
            {
                logger.Info("Error Occured. Method name ExportMapping. " + ex.InnerException.ToString());
                result = "EXPORT MAPPING FAILED";
                fnReturnValue = ReturnCode.EXCEPTION_ENCOUNTERED;
                SendNotification("Error Occured. Method name ExportMapping. ", ex.InnerException.ToString());
            }

            stopwatch.Stop();
            var elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
            logger.Info("Time Finsihed: " + DateTime.Now.ToString());
            logger.Info("Total processing time: " + elapsed_time + " seconds");
            logger.Info(result);
            logger.Info("-------------------------------------------------------");
            logger.Info("-------------------------------------------------------");
            logger.Info("----------------------- End File-----------------------");
            logger.Info("");
            return fnReturnValue;
        }

        public DataTable GetImportExportMonitoringRulee()
        {
            SVADBConnStr = config.SVAConnectionString;
            SVADAO sdb = new SVADAO(SVADBConnStr);

            DataTable dt = new DataTable();
            dt = sdb.GetImportExportMonitoringRule();

            //if (dt.Rows.Count == 0)
            //{
            //    logger.Info("Method Name: GetImportExportMonitoringRule Query returned 0 results");
            //}

            return dt;
        }

        public DateTime GetLastActiveTime()
        {
            SVADAO dao = new SVADAO(SVADBConnStr);

            return dao.GetLastActiveTime();
        }

        public void UpdateNextRunSchedule(string Func)
        {
            SVADAO dao = new SVADAO(SVADBConnStr);

            dao.UpdateNextRunSchedule(Func);
        }

        public void SetCouponsToExported()
        {
            SVADAO dao = new SVADAO(SVADBConnStr);

            dao.SetCouponsToExported();
        }

        private void SendNotification(string failedtask, string message)
        {
            AppFunctions af = new AppFunctions();
            config = AppConfig.GetSingleton();
            string sendfrom = config.SendFrom;
            string receipient = config.EmailReceipients;
            string password = config.EmailPassword;
            string smtphost = config.SMTPHost;
            string smtpport = config.SMTPPort;

            string subject = "#996 Windows Service Notification";
            string body = "Dear #996 admin, " +
                Environment.NewLine +
                Environment.NewLine +
                "This is to notify you of activity or error in #996 Windows Service with details below" +
                Environment.NewLine +
                Environment.NewLine +
                failedtask + ", " + message +
                Environment.NewLine +
                Environment.NewLine +
                "";

            af.SendEmail(subject, body, sendfrom, receipient, password, smtphost, smtpport);
        }
       
    }
}
