﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Edge.SVA.ImportMain.Constants;
using Common;


namespace Edge.SVA.ImportMain
{
    public class BusinessLogic
    {
        string SVADBConnStr = "";
        AppConfig config;
        public BusinessLogic(string SVADBStr)
        {
            config = AppConfig.GetSingleton();
            SVADBConnStr = SVADBStr;
        }

        //public DateTime ComputeCardExpiryDate(int RRC_Type_Of_Application, string RRC_Customer_Tier, string ApplicationDate, string OldCardExpiryDate)
        //{
        //    DateTime ExpirationDate = DateTime.Now;
        //    //    int yearstoadd = dao.GetCardExpirationDate(RRC_Type_Of_Application, RRC_Customer_Tier);

        //    //    if (RRC_Type_Of_Application == RRC_Type_of_Application.REPLACEMENT)
        //    if (RRC_Type_Of_Application == RRC_Type_of_Application.NEW)
        //    {
        //        if (RRC_Customer_Tier != CustomerTier.BEEP) //classic or elite
        //        {
        //            ExpirationDate =  DateTime.Parse(ApplicationDate).AddYears(2); //add 2 years to expiration date
        //        }

        //        else //beep
        //        {
        //            ExpirationDate = DateTime.Parse(ApplicationDate).AddYears(4); //add 4 years to expiration date
        //        }
        //    }
        //    else if (RRC_Type_Of_Application == RRC_Type_of_Application.RENEWAL)
        //    {
        //        if (RRC_Customer_Tier != CustomerTier.BEEP) //classic or elite
        //        {
        //            ExpirationDate = DateTime.Parse(OldCardExpiryDate).AddYears(2); //add 2 years to expiration date
        //        }

        //        else //beep
        //        {
        //            ExpirationDate = DateTime.Parse(OldCardExpiryDate).AddYears(4); //add 4 years to expiration date
        //        }
        //    }
        //    else if (RRC_Type_Of_Application == RRC_Type_of_Application.REPLACEMENT)
        //    {

        //        var DateSpan = DateTimeSpan.CompareDates(DateTime.Parse(OldCardExpiryDate), DateTime.Parse(ApplicationDate));
        //        int MonthDiff = DateSpan.Months;

        //        if (MonthDiff > 6) //month difference is more than 6 months
        //        {
        //            ExpirationDate = DateTime.Parse(OldCardExpiryDate); //Old expiration date applies since card to be replaced is close to expiration
        //        }
        //        else // month difference <= 6 months
        //        {
        //            if (RRC_Customer_Tier != CustomerTier.BEEP) //classic or elite
        //            {
        //                ExpirationDate = DateTime.Parse(OldCardExpiryDate).AddYears(2); //add 2 years to expiration date
        //            }
        //            else //beep
        //            {
        //                ExpirationDate = DateTime.Parse(OldCardExpiryDate).AddYears(4); //add 4 years to expiration date
        //            }

        //        }
        //    }

        //    return ExpirationDate;
        //}

    //    public DateTime ComputeCardExpiryDate(int RRC_Type_Of_Application, string RRC_Customer_Tier, string ApplicationDate, string OldCardExpiryDate)
    //    {
    //        DateTime ExpirationDate = DateTime.Now;

    //        SVADBConnStr = config.SVAConnectionString;
    //        SVADAO dao = new SVADAO(SVADBConnStr);

    //        int yearstoadd = dao.GetCardExpirationDate(RRC_Type_Of_Application, RRC_Customer_Tier);

    //        if (RRC_Type_Of_Application == RRC_Type_of_Application.NEW)
    //        {
    //            ExpirationDate = this.SetDateToEOM(DateTime.Parse(ApplicationDate).AddYears(yearstoadd));
    //        }
    //        else if (RRC_Type_Of_Application == RRC_Type_of_Application.REPLACEMENT)
    //        {

    //            var DateSpan = DateTimeSpan.CompareDates(DateTime.Parse(ApplicationDate),DateTime.Parse(OldCardExpiryDate));
    //            int MonthDiff = DateSpan.Months;

    //            int DaysDiff = (DateTime.Parse(OldCardExpiryDate) - DateTime.Parse(ApplicationDate)).Days;

    //            if (DaysDiff > 243)//(MonthDiff > 6) //month difference is more than 6 months
    //            {
    //                ExpirationDate = DateTime.Parse(OldCardExpiryDate); //Old expiration date applies since card to be replaced is close to expiration
    //            }
    //            else // month difference <= 6 months
    //            {
    //                ExpirationDate = DateTime.Parse(OldCardExpiryDate).AddYears(yearstoadd);
    //            }

    //        }
    //        else
    //        {
    //            ExpirationDate = DateTime.Parse(OldCardExpiryDate).AddYears(yearstoadd);
    //        }



    //        return ExpirationDate;
    //    }

    //    public DateTime SetDateToEOM(DateTime date1)
    //    {
    //        int daysInMonth = System.DateTime.DaysInMonth(date1.Year, date1.Month);

    //        int dayOfMonth = date1.Day;



    //        return date1.AddDays(daysInMonth - dayOfMonth);
    //    }

    //}

    //public struct DateTimeSpan
    //{
    //    private readonly int years;
    //    private readonly int months;
    //    private readonly int days;
    //    private readonly int hours;
    //    private readonly int minutes;
    //    private readonly int seconds;
    //    private readonly int milliseconds;

    //    public DateTimeSpan(int years, int months, int days, int hours, int minutes, int seconds, int milliseconds)
    //    {
    //        this.years = years;
    //        this.months = months;
    //        this.days = days;
    //        this.hours = hours;
    //        this.minutes = minutes;
    //        this.seconds = seconds;
    //        this.milliseconds = milliseconds;
    //    }

    //    public int Years { get { return years; } }
    //    public int Months { get { return months; } }
    //    public int Days { get { return days; } }
    //    public int Hours { get { return hours; } }
    //    public int Minutes { get { return minutes; } }
    //    public int Seconds { get { return seconds; } }
    //    public int Milliseconds { get { return milliseconds; } }

    //    enum Phase { Years, Months, Days, Done }

    //    public static DateTimeSpan CompareDates(DateTime date1, DateTime date2)
    //    {
    //        if (date2 < date1)
    //        {
    //            var sub = date1;
    //            date1 = date2;
    //            date2 = sub;
    //        }

    //        DateTime current = date1;
    //        int years = 0;
    //        int months = 0;
    //        int days = 0;

    //        Phase phase = Phase.Years;
    //        DateTimeSpan span = new DateTimeSpan();
    //        int officialDay = current.Day;

    //        while (phase != Phase.Done)
    //        {
    //            switch (phase)
    //            {
    //                case Phase.Years:
    //                    if (current.AddYears(years + 1) > date2)
    //                    {
    //                        phase = Phase.Months;
    //                        current = current.AddYears(years);
    //                    }
    //                    else
    //                    {
    //                        years++;
    //                    }
    //                    break;
    //                case Phase.Months:
    //                    if (current.AddMonths(months + 1) > date2)
    //                    {
    //                        phase = Phase.Days;
    //                        current = current.AddMonths(months);
    //                        if (current.Day < officialDay && officialDay <= DateTime.DaysInMonth(current.Year, current.Month))
    //                            current = current.AddDays(officialDay - current.Day);
    //                    }
    //                    else
    //                    {
    //                        months++;
    //                    }
    //                    break;
    //                case Phase.Days:
    //                    if (current.AddDays(days + 1) > date2)
    //                    {
    //                        current = current.AddDays(days);
    //                        var timespan = date2 - current;
    //                        span = new DateTimeSpan(years, months, days, timespan.Hours, timespan.Minutes, timespan.Seconds, timespan.Milliseconds);
    //                        phase = Phase.Done;
    //                    }
    //                    else
    //                    {
    //                        days++;
    //                    }
    //                    break;
    //            }
    //        }

    //        return span;
    //    }
    }
}
