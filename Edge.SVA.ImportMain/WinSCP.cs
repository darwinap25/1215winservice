﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Common;
using Edge.SVA.ImportMain;
using Edge.SVA.ImportMain.Constants;
using System.Threading;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Configuration;
using WinSCP;
using log4net;
namespace Edge.SVA.ImportMain
{
    public class WinSCP
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        string SVADBConnStr = "";
        AppConfig config;


        public WinSCP()
        {
        }


        public int TransferBaggageTag()
        {
            AppConfig config;
            config = AppConfig.GetSingleton();

            logger.Info("");
            logger.Info("------------------ SFTP Transfer Baggage Ta---------------");
            logger.Info("----------------------------------------------------------");
            logger.Info("----------------------------------------------------------");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            logger.Info("Time Started: " + DateTime.Now.ToString());
            logger.Info("FTPExportPath:" + config.FTPExportPath);
            logger.Info("ExportPath:" + config.ExportPath);
            string result = "";
            var elapsed_time = long.Parse("0");
            try
            {
              

                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    //Protocol = Protocol.Ftp,
                    HostName = config.FTPHostName,
                    UserName = config.FTPUserName,
                    Password = config.FTPPassword,
                    PortNumber = int.Parse(config.FTPPortNumber),
                    FtpMode = FtpMode.Passive,
                    SshHostKeyFingerprint = config.FTPSshHostKeyFingerprint


                    //FtpMode = FtpMode.Passive,
                    //FtpSecure = FtpSecure.Explicit,
                    //TlsHostCertificateFingerprint = config.FTPSshHostKeyFingerprint



                    //Protocol = Protocol.Ftp,
                    //HostName = config.FTPHostName,
                    //UserName = config.FTPUserName,
                    //Password = config.FTPPassword,
                    //PortNumber = int.Parse(config.FTPPortNumber),
                    //FtpMode = FtpMode.Active
                };

                string exportpath = config.ExportPath + "\\*";
                string ftpexportpath = config.FTPExportPath;
                string BakPath = config.BakPath;

                this.CopyContentsOfDirectory(config.ExportPath, BakPath + "BaggageTag\\");
                
                
                using (Session session = new Session())
                {
                    // Connect
                    session.Open(sessionOptions);
                    
                    //session.Timeout = TimeSpan.FromSeconds(5);
                    logger.Info("Connected to SFTP Server");
                    logger.Info("SFTP - Connected to SFTP Server");

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;
                    transferOptions.FilePermissions = null; // This is default
                    transferOptions.PreserveTimestamp = false;

                    TransferOperationResult transferResult;
                    transferResult = session.PutFiles(@exportpath, ftpexportpath, true, transferOptions);
                    logger.Info("SFTP - Transfer of " + transferResult.ToString());

                    // Throw on any error
                    //transferResult.Check();

                    // Print results
                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        logger.Info("Transfer of " + transfer.FileName + " succeeded" );
                        logger.Info("SFTP - Transfer of " + transfer.FileName + " succeeded");
                    }

                    session.Close();
                    logger.Info("Connection to SFTP server closed");
                }

                stopwatch.Stop();
                elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
                logger.Info("Time Finsihed: " + DateTime.Now.ToString());
                logger.Info("Total processing time: " + elapsed_time + " seconds");
                result = "TANSFER BAGGAGE TAG SUCCESSFUL";
                logger.Info("SFTP " + result);
                logger.Info(result);
                logger.Info("-------------------------------------------------------");
                logger.Info("-------------------------------------------------------");
                logger.Info("----------------------- End File-----------------------");
                logger.Info("");
                
                return 0;
            }
            catch (Exception e)
            {
                stopwatch.Stop();
                elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
                logger.Info("Time Finsihed: " + DateTime.Now.ToString());
                logger.Info("Total processing time: " + elapsed_time + " seconds");
                result = "TRANSFER BAGGAGE TAG FAILED";
                logger.Info("SFTP " + result + ", " + e.ToString());
                logger.Info("Error: {0}", e);
                logger.Info(result);
                logger.Info("-------------------------------------------------------");
                logger.Info("-------------------------------------------------------");
                logger.Info("----------------------- End File-----------------------");
                logger.Info("");
                SendNotification("TRANSFER BAGGAGE TAG FAILED. Error:", e.Message.ToString() + "," + e.ToString());
                return 1;


            }

            
        }

        public int TransferMapping()
        {
            AppConfig config;
            config = AppConfig.GetSingleton();

            logger.Info("");
            logger.Info("---------------------SFTP Transfer Mapping ---------------");
            logger.Info("----------------------------------------------------------");
            logger.Info("----------------------------------------------------------");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            logger.Info("Time Started: " + DateTime.Now.ToString());
            logger.Info("FTPExportPath:" + config.FTPExportPath2);
            logger.Info("ExportPath:" + config.ExportPath2);
            string result = "";
            var elapsed_time = long.Parse("0");

            try
            {
             
                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    //Protocol = Protocol.Ftp,
                    HostName = config.FTPHostName,
                    UserName = config.FTPUserName,
                    Password = config.FTPPassword,
                    PortNumber = int.Parse(config.FTPPortNumber),
                    FtpMode = FtpMode.Passive,
                    SshHostKeyFingerprint = config.FTPSshHostKeyFingerprint

                    //FtpMode = FtpMode.Passive,
                    //FtpSecure = FtpSecure.Explicit,
                    //TlsHostCertificateFingerprint = config.FTPSshHostKeyFingerprint

                    //Protocol = Protocol.Ftp,
                    //HostName = config.FTPHostName,
                    //UserName = config.FTPUserName,
                    //Password = config.FTPPassword,
                    //PortNumber = int.Parse(config.FTPPortNumber),
                    //FtpMode = FtpMode.Active
                };

                string exportpath = config.ExportPath2 + "\\*";
                string ftpexportpath = config.FTPExportPath2;

                string BakPath = config.BakPath;

                this.CopyContentsOfDirectory(config.ExportPath, BakPath + "BaggageTag\\");

                using (Session session = new Session())
                {
                    // Connect
                    session.Open(sessionOptions);
                    logger.Info("Connected to SFTP Server");
                    logger.Info("SFTP - Connected to SFTP Server");

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;
                    transferOptions.FilePermissions = null; // This is default
                    transferOptions.PreserveTimestamp = false;

                    TransferOperationResult transferResult;
                    transferResult = session.PutFiles(@exportpath, ftpexportpath, true, transferOptions);

                    // Throw on any error
                    //transferResult.Check();

                    // Print results
                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        logger.Info("Transfer of " + transfer.FileName + " succeeded");
                    }
                    session.Close();
                    logger.Info("Connection to SFTP server closed");
                }

                stopwatch.Stop();
                elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
                logger.Info("Time Finsihed: " + DateTime.Now.ToString());
                logger.Info("Total processing time: " + elapsed_time + " seconds");
                result = "TRANSFER MAPPING SUCCESSFUL";
                logger.Info("SFTP " + result);
                logger.Info(result);
                logger.Info("-------------------------------------------------------");
                logger.Info("-------------------------------------------------------");
                logger.Info("----------------------- End File-----------------------");
                logger.Info("");

                return 0;
            }
            catch (Exception e)
            {
                stopwatch.Stop();
                elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
                logger.Info("Time Finsihed: " + DateTime.Now.ToString());
                logger.Info("Total processing time: " + elapsed_time + " seconds");
                result = "TANSFER MAPPING FAILED";
                logger.Info("SFTP " + result + ", " + e.ToString());
                logger.Info("Error: {0}", e);
                logger.Info(result);
                logger.Info("-------------------------------------------------------");
                logger.Info("-------------------------------------------------------");
                logger.Info("----------------------- End File-----------------------");
                logger.Info("");
                SendNotification("TRANSFER MAPPING FAILED. Error:", e.Message.ToString() + "," + e.ToString());
                return 1;
            }
        }


        public int GetAirline()
        {
            AppConfig config;
            config = AppConfig.GetSingleton();

            logger.Info("");
            logger.Info("---------------------SFTP Import Airline ---------------");
            logger.Info("----------------------------------------------------------");
            logger.Info("----------------------------------------------------------");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            logger.Info("Time Started: " + DateTime.Now.ToString());
            logger.Info("FTPImportPath:" + config.FTPImportPath);
            logger.Info("ImportPath:" + config.ImportPath);
            string result = "";
            var elapsed_time = long.Parse("0");


            try
            {

                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    //Protocol = Protocol.Ftp,
                    HostName = config.FTPHostName,
                    UserName = config.FTPUserName,
                    Password = config.FTPPassword,
                    PortNumber = int.Parse(config.FTPPortNumber),
                    FtpMode = FtpMode.Passive,
                    SshHostKeyFingerprint = config.FTPSshHostKeyFingerprint
                    //FtpMode = FtpMode.Passive,
                    //FtpSecure = FtpSecure.Explicit,
                    //TlsHostCertificateFingerprint = config.FTPSshHostKeyFingerprint

                    //Protocol = Protocol.Ftp,
                    //HostName = config.FTPHostName,
                    //UserName = config.FTPUserName,
                    //Password = config.FTPPassword,
                    //PortNumber = int.Parse(config.FTPPortNumber),
                    //FtpMode = FtpMode.Active
                    
                };

                string importpath = config.ImportPath;
                string ftpimportpath = config.FTPImportPath;

                using (Session session = new Session())
                {
                    // Connect
                    session.Open(sessionOptions);
                    logger.Info("Connected to SFTP Server");
                    logger.Info("SFTP - Connected to SFTP Server");

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;
                    transferOptions.FilePermissions = null; // This is default
                    transferOptions.PreserveTimestamp = false;

                    TransferOperationResult transferResult;
                    transferResult = session.GetFiles(@ftpimportpath, importpath, true, transferOptions);
                    
                    // Throw on any error
                    transferResult.Check();

                    // Print results
                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        logger.Info("Import of " + transfer.FileName + " succeeded");
                    }
                    session.Close();
                    logger.Info("Connection to SFTP server closed");
                }
                stopwatch.Stop();
                elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
                logger.Info("Time Finsihed: " + DateTime.Now.ToString());
                logger.Info("Total processing time: " + elapsed_time + " seconds");
                result = "IMPORT AIRLINE SUCCESSFUL";
                logger.Info("SFTP " + result);
                logger.Info(result);
                logger.Info("-------------------------------------------------------");
                logger.Info("-------------------------------------------------------");
                logger.Info("----------------------- End File-----------------------");
                logger.Info("");

                return 0;
            }
            catch (Exception e)
            {
                stopwatch.Stop();
                elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
                logger.Info("Time Finsihed: " + DateTime.Now.ToString());
                logger.Info("Total processing time: " + elapsed_time + " seconds");
                result = "IMPORT AIRLINE FAILED";
                logger.Info("SFTP " +  result + ", " + e.ToString());
                logger.Info("Error: {0}", e);
                logger.Info(result);
                logger.Info("-------------------------------------------------------");
                logger.Info("-------------------------------------------------------");
                logger.Info("----------------------- End File-----------------------");
                logger.Info("");
                SendNotification("IMPORT AIRLINE FAILED. Error:", e.Message.ToString() + "," + e.ToString());

                return 1;
            }
        }

        public int GetLostTags()
        {
            AppConfig config;
            config = AppConfig.GetSingleton();

            logger.Info("");
            logger.Info("---------------------SFTP Import Lost Tags ---------------");
            logger.Info("----------------------------------------------------------");
            logger.Info("----------------------------------------------------------");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            logger.Info("Time Started: " + DateTime.Now.ToString());
            logger.Info("FTPImportPath:" + config.FTPImportPath2);
            logger.Info("ImportPath:" + config.ImportPath2);
            string result = "";
            var elapsed_time = long.Parse("0");


            try
            {

                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    //Protocol = Protocol.Ftp,
                    HostName = config.FTPHostName,
                    UserName = config.FTPUserName,
                    Password = config.FTPPassword,
                    PortNumber = int.Parse(config.FTPPortNumber),
                    FtpMode = FtpMode.Passive,
                    SshHostKeyFingerprint = config.FTPSshHostKeyFingerprint

                    //FtpMode = FtpMode.Passive,
                    //FtpSecure = FtpSecure.Explicit,
                    //TlsHostCertificateFingerprint = config.FTPSshHostKeyFingerprint

                    //Protocol = Protocol.Ftp,
                    //HostName = config.FTPHostName,
                    //UserName = config.FTPUserName,
                    //Password = config.FTPPassword,
                    //PortNumber = int.Parse(config.FTPPortNumber),
                    //FtpMode = FtpMode.Active
                };

                string importpath = config.ImportPath2;
                string ftpimportpath = config.FTPImportPath2;

                using (Session session = new Session())
                {
                    // Connect
                    session.Open(sessionOptions);
                    logger.Info("Connected to SFTP Server");
                    logger.Info("SFTP - Connected to SFTP Server");

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;
                    transferOptions.FilePermissions = null; // This is default
                    transferOptions.PreserveTimestamp = false;

                    TransferOperationResult transferResult;
                    transferResult = session.GetFiles(@ftpimportpath, importpath, true, transferOptions);

                    // Throw on any error
                    transferResult.Check();

                    // Print results
                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        logger.Info("Import of " + transfer.FileName + " succeeded");
                    }
                    session.Close();
                    logger.Info("Connection to SFTP server closed");
                }
                stopwatch.Stop();
                elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
                logger.Info("Time Finsihed: " + DateTime.Now.ToString());
                logger.Info("Total processing time: " + elapsed_time + " seconds");
                result = "IMPORT LOST TAGS SUCCESSFUL";
                logger.Info("SFTP " + result );
                logger.Info(result);
                logger.Info("-------------------------------------------------------");
                logger.Info("-------------------------------------------------------");
                logger.Info("----------------------- End File-----------------------");
                logger.Info("");

                return 0;
            }
            catch (Exception e)
            {
                stopwatch.Stop();
                elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
                logger.Info("Time Finsihed: " + DateTime.Now.ToString());
                logger.Info("Total processing time: " + elapsed_time + " seconds");
                result = "IMPORT LOST TAGS FAILED";
                logger.Info("SFTP " + result + ", " + e.ToString());
                logger.Info("Error: {0}", e);
                logger.Info(result);
                logger.Info("-------------------------------------------------------");
                logger.Info("-------------------------------------------------------");
                logger.Info("----------------------- End File-----------------------");
                logger.Info("");
                SendNotification("IMPORT LOST TAGS FAILED. Error:", e.Message.ToString() + "," + e.ToString());
                return 1;
            }
        }

        public int GetFoundTags()
        {
            AppConfig config;
            config = AppConfig.GetSingleton();

            logger.Info("");
            logger.Info("---------------------SFTP Import Found Tags ---------------");
            logger.Info("----------------------------------------------------------");
            logger.Info("----------------------------------------------------------");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            logger.Info("Time Started: " + DateTime.Now.ToString());
            logger.Info("FTPImportPath:" + config.FTPImportPath3);
            logger.Info("ImportPath:" + config.ImportPath3);
            string result = "";
            var elapsed_time = long.Parse("0");


            try
            {

                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    //Protocol = Protocol.Ftp,
                    HostName = config.FTPHostName,
                    UserName = config.FTPUserName,
                    Password = config.FTPPassword,
                    PortNumber = int.Parse(config.FTPPortNumber),
                    FtpMode = FtpMode.Passive,
                    SshHostKeyFingerprint = config.FTPSshHostKeyFingerprint

                    //FtpMode = FtpMode.Passive,
                    //FtpSecure = FtpSecure.Explicit,
                    //TlsHostCertificateFingerprint = config.FTPSshHostKeyFingerprint

                    //Protocol = Protocol.Ftp,
                    //HostName = config.FTPHostName,
                    //UserName = config.FTPUserName,
                    //Password = config.FTPPassword,
                    //PortNumber = int.Parse(config.FTPPortNumber),
                    //FtpMode = FtpMode.Active
                };

                string importpath = config.ImportPath3;
                string ftpimportpath = config.FTPImportPath3;

                using (Session session = new Session())
                {
                    // Connect
                    session.Open(sessionOptions);
                    logger.Info("Connected to SFTP Server");
                    logger.Info("SFTP - Connected to SFTP Server");

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;
                    transferOptions.FilePermissions = null; // This is default
                    transferOptions.PreserveTimestamp = false;

                    TransferOperationResult transferResult;
                    transferResult = session.GetFiles(@ftpimportpath, importpath, true, transferOptions);

                    // Throw on any error
                    transferResult.Check();

                    // Print results
                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        logger.Info("Import of " + transfer.FileName + " succeeded");
                    }
                    session.Close();
                    logger.Info("Connection to SFTP server closed");
                }
                stopwatch.Stop();
                elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
                logger.Info("Time Finsihed: " + DateTime.Now.ToString());
                logger.Info("Total processing time: " + elapsed_time + " seconds");
                result = "IMPORT FOUND TAGS SUCCESSFUL";
                logger.Info("SFTP " + result);
                logger.Info(result);
                logger.Info("-------------------------------------------------------");
                logger.Info("-------------------------------------------------------");
                logger.Info("----------------------- End File-----------------------");
                logger.Info("");

                return 0;
            }
            catch (Exception e)
            {
                stopwatch.Stop();
                elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
                logger.Info("Time Finsihed: " + DateTime.Now.ToString());
                logger.Info("Total processing time: " + elapsed_time + " seconds");
                result = "IMPORT FOUND TAGS FAILED";
                logger.Info("SFTP " + result);
                logger.Info("Error: {0}", e);
                logger.Info(result);
                logger.Info("-------------------------------------------------------");
                logger.Info("-------------------------------------------------------");
                logger.Info("----------------------- End File-----------------------");
                logger.Info("");
                SendNotification("IMPORT FOUND TAGS FAILED. Error:", e.Message.ToString() + "," + e.ToString());
                return 1;
            }
        }

        //public void UpdateNextRunSchedule(string Func)
        //{
        //    SVADAO dao = new SVADAO(SVADBConnStr);

        //    dao.UpdateNextRunSchedule(Func);
        //}

        private void CopyContentsOfDirectory(string PathName, string DestinationPath)
        {
          
            string filename1 = "";
            string filename2 = "";
            try
            {
                if (Directory.Exists(PathName))
                {
                    

                    int lastp = PathName.LastIndexOf("\\");
                    string temp = PathName.Substring(lastp + 1, PathName.Length - lastp - 1);

                    string[] filenames = Directory.GetFiles(PathName);
                    foreach (string fname in filenames)
                    {
                        if (File.Exists(fname))
                        {
                            filename1 = fname;
                            filename2 = DestinationPath + Path.GetFileName(fname);
                            System.IO.File.Copy(filename1, filename2);
                        }
                    }
                    
                    
                }
            }
            catch (System.Exception ex)
            {
                logger.Info("ImportExport Server - Error Occured - CopyContentsOfDirectory:" + ex.InnerException.ToString());
                logger.Info("Error Occured, CopyContentsOfDirectory: " + ex.StackTrace.ToString());
                SendNotification("Error Occured, CopyContentsOfDirectory:", ex.InnerException.ToString().ToString());

            }
  
        }

        public bool IsDirectoryEmpty(string path)
        {
            return IsDirectoryEmpty(new DirectoryInfo(path));
        }

        public bool IsDirectoryEmpty(DirectoryInfo directory)
        {
            FileInfo[] files = directory.GetFiles();
            DirectoryInfo[] subdirs = directory.GetDirectories();

            return (files.Length == 0 && subdirs.Length == 0);
        }

        private void SendNotification(string failedtask, string message)
        {
            //AppFunctions af = new AppFunctions();
            //config = AppConfig.GetSingleton();
            //string sendfrom = config.SendFrom;
            //string receipient = config.EmailReceipients;
            //string password = config.EmailPassword;
            //string smtphost = config.SMTPHost;
            //string smtpport = config.SMTPPort;

            //string subject = "#996 Windows Service Notification";
            //string body = "Dear #996 admin, " +
            //    Environment.NewLine +
            //    Environment.NewLine +
            //    "This is to notify you of activity or error in #996 Windows Service with details below" +
            //    Environment.NewLine +
            //    Environment.NewLine +
            //    failedtask + ", " + message +
            //    Environment.NewLine +
            //    Environment.NewLine +
            //    "";

            //af.SendEmail(subject, body, sendfrom, receipient, password, smtphost, smtpport);
        }
 
    }
}
