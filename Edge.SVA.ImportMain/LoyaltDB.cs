﻿using System;
using System.Data;
using Edge.SVA.ImportMain.Constants;

namespace Edge.SVA.ImportMain
{
    public class LoyaltDB
    {
        DatabaseUtil.Interface.IDatabase database;
        public LoyaltDB(string connstr)
        {
            DatabaseUtil.Factory.SetConnecctionString(connstr);
            DatabaseUtil.Factory.SetDefaultTimeout(600);
            database = DatabaseUtil.Factory.CreateIDatabase();
        }

        
        public string GetMemberInternalKey(string CardNumber, ref DateTime ExpirationDate, ref DateTime ExpirationDate2)
        {
            string MemberInternalKey = "";
            string queryString = "Select ClubCardId,MemberInternalKey, ExpirationDate from CRM_Clubcard where ClubCardId = '" + CardNumber + "'";

            DataTable dt = database.Query(queryString, "dt");
            if (dt.Rows.Count > 0)
            {
                MemberInternalKey = dt.Rows[0]["MemberInternalKey"].ToString();
                ExpirationDate = DateTime.Parse(dt.Rows[0]["ExpirationDate"].ToString());
                ExpirationDate2 = ExpirationDate.AddYears(2);
            }            
            return MemberInternalKey;
        }

        public string GetBuyingUnitInternalKey(string CardNumber, string MemberInternalKey, int rrctype)
        {
            string BuyingUnitInternalKey = "";
            if (MemberInternalKey == "")
                MemberInternalKey = "0";
            //string queryString = "Select BuyingUnitInternalKey from CRM_Member where MemberInternalKey='" + MemberInternalKey + "' and ExternalMemberKey='" + CardNumber + "'";
            string queryString = "";
            if (rrctype == 1)
            {
                queryString = "Select top 1 BuyingUnitInternalKey from CRM_Member Inner Join" +
                                "where MemberInternalKey=" + MemberInternalKey + " and IsMainMember=1 order by BuyingUnitInternalKey desc";
            }
            else
            {
                queryString = "Select top 1 BuyingUnitInternalKey from CRM_Member where MemberInternalKey=" + MemberInternalKey + " order by BuyingUnitInternalKey desc";
            }
            DataTable dt = database.Query(queryString, "dt");
            if (dt.Rows.Count > 0)
            {
                BuyingUnitInternalKey = dt.Rows[0]["BuyingUnitInternalKey"].ToString();
            }
            return BuyingUnitInternalKey;
        }

        public string GetExternalBuyingUnit(string CardNumber, string BuyingUnitInternalKey)
        {
            //SqlConnection sqlconn = new SqlConnection(ConnStr);     
            //sqlconn.Open();
            string ExternalBuyingUnit = "";
            string queryString = "Select ExternalBuyingUnit from CRM_BuyingUnit where BuyingUnitInternalKey='" + BuyingUnitInternalKey + "'";
            DataTable dt = database.Query(queryString, "dt");
            if (dt.Rows.Count > 0)
            {
                ExternalBuyingUnit = dt.Rows[0]["ExternalBuyingUnit"].ToString();
            }
            return ExternalBuyingUnit;
        }

        public void GetBalance(string CardNumber, string BuyingUnitInternalKey, string AccountInternalKey, ref string balance)
        {            
            balance = "0";
            string queryString = "Select AccountInternalKey, Balance from CRM_BuyingUnitAccountsActivity where BuyingUnitInternalKey='" + BuyingUnitInternalKey + "' and AccountInternalKey = '" + AccountInternalKey + "'"; 
            DataTable dt = database.Query(queryString, "dt");
            if (dt.Rows.Count > 0)
            {
                //AccountInternalKey = dt.Rows[0]["AccountInternalKey"].ToString();
                balance = dt.Rows[0]["balance"].ToString();
            }
        }


        public DataTable GetMemberDetails(string CardNumber, int rrctype)
        {
            string queryString;

            if (rrctype == RRC_Type_of_Application.NEW)
            {
                queryString = "SELECT TOP (1) m.MemberInternalKey, c.ClubCardId, c.RestrictionId, m.ExternalMemberKey, m.BuyingUnitInternalKey, c.ExpirationDate, b.ExternalBuyingUnit from CRM_Clubcard c"
                                + "INNER JOIN CRM_Member m ON c.MemberInternalKey = m.MemberInternalKey " 
                                + "INNER JOIN CRM_BuyingUnit b ON m.BuyingUnitInternalKey = b.BuyingUnitInternalKey"
                                + "WHERE c.ClubCardId = " + CardNumber + " and IsMainMember=1 order by BuyingUnitInternalKey desc";
            }
            else
            {
                queryString = "SELECT TOP (1) m.MemberInternalKey, c.ClubCardId, c.RestrictionId, m.ExternalMemberKey, m.BuyingUnitInternalKey, b.ExternalBuyingUnit from CRM_Clubcard c"
                                + "INNER JOIN CRM_Member m ON c.MemberInternalKey = m.MemberInternalKey "
                                + "INNER JOIN CRM_BuyingUnit b ON m.BuyingUnitInternalKey = b.BuyingUnitInternalKey"
                                + "WHERE c.ClubCardId = " + CardNumber + " order by BuyingUnitInternalKey desc";
            }

            DataTable dt = database.Query(queryString, "dt");

            return dt;
        }

        public DataTable GetOldMemberDetails(string BuyingUnitInternalKey)
        {
            string queryString = "Select ExternalMemberKey, MemberInternalKey, BuyingUnitInternalKey, BirthDate, MobilePhoneNumber, WorkPhoneNumber, Gender from CRM_Member where IsMainMember= 1 and BuyingUnitInternalKey='" + BuyingUnitInternalKey + "'";
            DataTable dt = database.Query(queryString, "dt");

            return dt;
        }

        public DataTable GetOldCard(string MemberInternalKey)
        {

            string queryString = "Select ExternalMemberKey, BuyingUnitInternalKey from CRM_Member where MemberInternalKey='" + MemberInternalKey + "'";
            DataTable dt = database.Query(queryString, "dt");

            return dt;
        }

        public DataTable GetHouseholdDetails(string CardNumber, string MemberInternalKey, int rrctype)
        {
            string BuyingUnitInternalKey;

            BuyingUnitInternalKey = this.GetBuyingUnitInternalKey(CardNumber, MemberInternalKey, rrctype);

            string queryString = "Select ExternalBuyingUnit, BuyingUnitInternalKey, PostalCode, Country, Street1, City, HomePhone, EMailAddress, SendEmail from CRM_BuyingUnit where BuyingUnitInternalKey='" + BuyingUnitInternalKey + "'";
            DataTable dt = database.Query(queryString, "dt");

            return dt;
        }

        public DataTable GetBuyingUnitDetails(string BuyingUnitInternalKey)
        {

            string queryString = "Select ExternalBuyingUnit, BuyingUnitInternalKey, PostalCode, Country, Street1, City, HomePhone, EMailAddress, SendEmail from CRM_BuyingUnit where MemberInternalKey='" + BuyingUnitInternalKey + "'";
            DataTable dt = database.Query(queryString, "dt");

            return dt;
        }
            
        public DataTable GetMembrAttributeValues(string MemberInternalKey)
        {

            string queryString = "Select AttributeId, StringValue, LongValue from CRM_MemberAttributeValue where MemberInternalKey='" + MemberInternalKey + "'";
            DataTable dt = database.Query(queryString, "dt");

            return dt;
        }

      
    }
}
