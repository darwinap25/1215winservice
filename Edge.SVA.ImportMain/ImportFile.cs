﻿using Common;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Edge.SVA.ImportMain.Constants;


namespace Edge.SVA.ImportMain
{
    public class ImportFile
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        string SVADBConnStr = "";
        AppConfig config;
        
        public ImportFile(string SVADBStr)
        {
            config = AppConfig.GetSingleton();


            SVADBConnStr = config.SVAConnectionString;
            //LoyaltyDBConnStr = LDBStr;
            //SVADBConnStr = SVADBStr;
        }


        public int ImportAirline(string filename)
        {
            int fnReturnValue = ReturnCode.EXCEPTION_ENCOUNTERED;
            logger.Info("");
            logger.Info("---------------------New Import Airline ---------------");
            logger.Info("-------------------------------------------------------");
            logger.Info("-------------------------------------------------------");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            logger.Info("Time Started: " + DateTime.Now.ToString());
            logger.Info("FullPath:" + filename);
            logger.Info("ImportPath:" + config.ImportPath);

            DataTable dt;
            DataTable dt2;
            CSVFileHelper cf = new CSVFileHelper();
            DateTime date = DateTime.Now;
            string result = "";
            logger.Info("Import Airline Start. Filename: " + filename + ". DateTime: " + DateTime.Now.ToString());
            if (System.IO.File.Exists(filename))
            {
                SVADAO dao = new SVADAO(SVADBConnStr);
                try
                {

                    dt = cf.OpenCSV(filename);
                    dt2 = dt.Clone();
                    int counter = 1;
                    foreach (DataRow dr in dt.Rows)
                    {
                        DataRow dr1 = dt2.NewRow();
                        foreach (DataColumn column in dt2.Columns)
                            dr1[column.ColumnName] = dr[column.ColumnName];
                        dt2.Rows.Add(dr1);

                        dao.InsertAirline(dr["Airline"].ToString());
                        counter++;
                    }
                    result = "IMPORT AIRLINE SUCCESS";
                    logger.Info("Import Airline Successful. Filename: " + filename);
                    fnReturnValue = ReturnCode.SUCCESSFUL;
          
                }

                catch (System.Exception ex)
                {
                    result = "IMPORT AIRLINE FAILED";
                    logger.Info("Error Occured. Method name ImportAirline. " + ex.InnerException.ToString());
                    fnReturnValue = ReturnCode.EXCEPTION_ENCOUNTERED;
                    SendNotification("Error Occured. Method name ImportAirline. ", ex.InnerException.ToString().ToString());
                }

                stopwatch.Stop();
                var elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
                logger.Info("Time Finsihed: " + DateTime.Now.ToString());
                logger.Info("Total processing time: " + elapsed_time + " seconds");
                logger.Info(result);
                logger.Info("-------------------------------------------------------");
                logger.Info("-------------------------------------------------------");
                logger.Info("----------------------- End File-----------------------");
                logger.Info("");

                
            }
            return fnReturnValue;
        }

        public int ImportLostTag(string filename)
        {
            int fnReturnValue = ReturnCode.EXCEPTION_ENCOUNTERED;
            logger.Info("");
            logger.Info("---------------------New Import Lost Tag ---------------");
            logger.Info("-------------------------------------------------------");
            logger.Info("-------------------------------------------------------");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            logger.Info("Time Started: " + DateTime.Now.ToString());
            logger.Info("FullPath:" + filename);
            logger.Info("ImportPath:" + config.ImportPath2);

            DataTable dt;
            DataTable dt2;
            CSVFileHelper cf = new CSVFileHelper();
            DateTime date = DateTime.Now;
            string result = "";
            logger.Info("Import Lost Tag Start. Filename: " + filename + ". DateTime: " + DateTime.Now.ToString());
            if (System.IO.File.Exists(filename))
            {
                SVADAO dao = new SVADAO(SVADBConnStr);
                try
                {

                    dt = cf.OpenCSV(filename);
                    dt2 = dt.Clone();
                    
                    //set all tags to active-inlocation before tagging them as lost
                    //not an ideal workflow but client want's it that way
                    dao.SetCouponsToIssuedInLocation(date); 

                    int counter = 1;
                    foreach (DataRow dr in dt.Rows)
                    {
                        DataRow dr1 = dt2.NewRow();
                        foreach (DataColumn column in dt2.Columns)
                            dr1[column.ColumnName] = dr[column.ColumnName];
                        dt2.Rows.Add(dr1);

                        //dao.InsertAirline(dr["Airline"].ToString());
                        dao.SetCouponsToLost(dr["BaggageTag"].ToString().Trim(), date);
                        counter++;
                    }
                    result = "IMPORT LOST TAG SUCCESS";
                    logger.Info("Import Lost Tag Successful. Filename: " + filename);
                    fnReturnValue = ReturnCode.SUCCESSFUL;

                }

                catch (System.Exception ex)
                {
                    result = "IMPORT LOST TAG FAILED";
                    logger.Info("Error Occured. Method name ImportLostTag. " + ex.InnerException.ToString());
                    fnReturnValue = ReturnCode.EXCEPTION_ENCOUNTERED;
                    SendNotification("Error Occured. Method name ImportLostTag. ", ex.InnerException.ToString());
                }

                stopwatch.Stop();
                var elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
                logger.Info("Time Finsihed: " + DateTime.Now.ToString());
                logger.Info("Total processing time: " + elapsed_time + " seconds");
                logger.Info(result);
                logger.Info("-------------------------------------------------------");
                logger.Info("-------------------------------------------------------");
                logger.Info("----------------------- End File-----------------------");
                logger.Info("");


            }
            return fnReturnValue;
        }

        public int ImportFoundTag(string filename)
        {
            int fnReturnValue = ReturnCode.EXCEPTION_ENCOUNTERED;
            logger.Info("");
            logger.Info("---------------------New Import Found Tag ---------------");
            logger.Info("-------------------------------------------------------");
            logger.Info("-------------------------------------------------------");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            logger.Info("Time Started: " + DateTime.Now.ToString());
            logger.Info("FullPath:" + filename);
            logger.Info("ImportPath:" + config.ImportPath3);

            DataTable dt;
            DataTable dt2;
            CSVFileHelper cf = new CSVFileHelper();
            DateTime date = DateTime.Now;
            string result = "";
            logger.Info("Import Found Tag Start. Filename: " + filename + ". DateTime: " + DateTime.Now.ToString());
            if (System.IO.File.Exists(filename))
            {
                SVADAO dao = new SVADAO(SVADBConnStr);
                try
                {

                    dt = cf.OpenCSV(filename);
                    dt2 = dt.Clone();
                    int counter = 1;
                    foreach (DataRow dr in dt.Rows)
                    {
                        DataRow dr1 = dt2.NewRow();
                        foreach (DataColumn column in dt2.Columns)
                            dr1[column.ColumnName] = dr[column.ColumnName];
                        dt2.Rows.Add(dr1);

                        //dao.InsertAirline(dr["Airline"].ToString());
                        dao.SetCouponsToIssuedInLocation(dr["BaggageTag"].ToString().Trim(), date);
                        counter++;
                    }
                    result = "IMPORT FOUND TAG SUCCESS";
                    logger.Info("Import Found Tag Successful. Filename: " + filename);
                    fnReturnValue = ReturnCode.SUCCESSFUL;

                }

                catch (System.Exception ex)
                {
                    result = "IMPORT FOUND TAG FAILED";
                    logger.Info("Error Occured. Method name ImportFoundTag. " + ex.InnerException.ToString());
                    fnReturnValue = ReturnCode.EXCEPTION_ENCOUNTERED;
                    SendNotification("Error Occured. Method name ImportFoundTag. ", ex.InnerException.ToString());
                }

                stopwatch.Stop();
                var elapsed_time = stopwatch.ElapsedMilliseconds / 1000;
                logger.Info("Time Finsihed: " + DateTime.Now.ToString());
                logger.Info("Total processing time: " + elapsed_time + " seconds");
                logger.Info(result);
                logger.Info("-------------------------------------------------------");
                logger.Info("-------------------------------------------------------");
                logger.Info("----------------------- End File-----------------------");
                logger.Info("");


            }
            return fnReturnValue;
        }

        public void UpdateNextRunSchedule(string Func)
        {
            SVADAO dao = new SVADAO(SVADBConnStr);

            dao.UpdateNextRunSchedule(Func);
        }

        private void SendNotification(string failedtask, string message)
        {
            AppFunctions af = new AppFunctions();
            config = AppConfig.GetSingleton();
            string sendfrom = config.SendFrom;
            string receipient = config.EmailReceipients;
            string password = config.EmailPassword;
            string smtphost = config.SMTPHost;
            string smtpport = config.SMTPPort;

            string subject = "HKIA TMS Windows Service Notification";
            string body = "Dear HKIA TMS admin, " +
                Environment.NewLine +
                Environment.NewLine +
                "This is to notify you of activity or error in HKIA TMS Windows Service with details below" +
                Environment.NewLine +
                Environment.NewLine +
                failedtask + ", " + message +
                Environment.NewLine +
                Environment.NewLine +
                "";

            af.SendEmail(subject, body, sendfrom, receipient, password, smtphost, smtpport);
        }
    }
}