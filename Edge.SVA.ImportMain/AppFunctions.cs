﻿using System;
using System.Collections.Generic;
using Common;
using System.Data;
using Edge.SVA.ImportServer.Model;
using Edge.SVA.ImportServer.Model1;
using log4net;
using Edge.SVA.ImportMain.Constants;
using System.Net.Mail;
using System.Net;

namespace Edge.SVA.ImportMain
{
    public class AppFunctions
    {
        public AppFunctions()
        { }

        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string ValidateStringWithDefaultValue(string StringToValidate, string DefaultValue)
        {
            string fnReturnValue = "";
            if (string.IsNullOrEmpty(StringToValidate))
            {
                fnReturnValue = DefaultValue;
            }
            else
            {
                fnReturnValue = StringToValidate;
            }

            return fnReturnValue;

        }

        public string ValidateFieldContent(string FieldName)
        {
            string fnReturnValue = "";
            if (string.IsNullOrEmpty(FieldName))
            {
                fnReturnValue = "";

                logger.Info(FieldName  + " is empty.");
            }
            else
            {
                fnReturnValue = FieldName;
            }

            return fnReturnValue;

        }

        

        public int ExportFile(Edge.SVA.ImportServer.Model.LoyaltyCustomer ly, string exportfile, string exportfilehead, string exportfileend)
        {
            try
            {
                string fn = GenFileName(exportfile, exportfilehead, exportfileend, 2);
                if (ly.Retailer.HouseHold != null)
                {
                    if (ly.Retailer.HouseHold.Count > 0)
                    {
                        XmlSerializer.SaveToXml(fn, ly, ly.GetType(), "LoyaltyCustomer");                        
                        //close XML file
                    }                    
                    else
                        logger.Debug("Household Count = 0. Nothing to export.");
                }
                else
                    logger.Debug("Household Count = null. Nothing to export.");
            }
            catch (System.Exception ex)
            { 
                logger.Debug("ExportFile Failed:" + ex.InnerException.ToString()); 
            }

            //Open exported XML and delete all special containers in Exported XML
            XmlSerializer.cleanXML(exportfile);
            return 0;
        }

        public int ExportFile(Edge.SVA.ImportServer.Model1.LoyaltyCustomer ly, string exportfile, string exportfilehead, string exportfileend)
        {
            try
            {
                string fn = GenFileName(exportfile, exportfilehead, exportfileend, 2);
                if (ly.Retailer.HouseHold != null)
                {
                    if (ly.Retailer.HouseHold.Count > 0)
                    {
                        XmlSerializer.SaveToXml(fn, ly, ly.GetType(), "LoyaltyCustomer");
                    }
                    else
                        logger.Debug("Household Count = 0. Nothing to export.");
                }
                else
                    logger.Debug("HouseHold == null. Nothing to export.");
            }
            catch (System.Exception ex)
            { 
                logger.Debug("ExportFile Failed:" + ex.InnerException.ToString()); 
            }

            return 0;
        }

        public int ExportFile(Edge.SVA.ImportServer.Model.Root r, string exportfile, string exportfilehead, string exportfileend)
        {
            string fn = exportfile;
            if (r != null)
            {            
                XmlSerializer.SaveToXml(fn, r, r.GetType(), "Root");
            }
            return 0;
        }

        public int ExportFile(Root cm, string filename)
        {
            string fn = filename;
            if (cm != null)
            {             
                XmlSerializer.SaveToXml(fn, cm, cm.GetType(), "Root");
            }
            return 0;
        }

        private string GenFileName(string filename, string filnameehead, string filenameend, int seqlen)
        {
            string file = filename;
            int i = 1;
            string seq = "";
            int len = 0;
            while (System.IO.File.Exists(file))
            {
                seq = "0000000000" + i.ToString();
                //logger.Debug("GenFileName  seq:" + seq + "  seq length:" + seq.Length.ToString());
                len = (seq.Length - seqlen - 1);
                seq = seq.Substring(len, seqlen);
                file = filnameehead + seq + filenameend;
                //logger.Debug("GenFileName:" + file);
                i++;
            }
            return file;
        }

        public void SendEmail(string subject, string body, string sendfrom, string receipient,
                                string password, string smtphost, string smtpport)
        {

            try
            {
                MailMessage mail = new MailMessage(sendfrom, receipient);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = false;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = smtphost;
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential(sendfrom, password);
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = NetworkCred;
                smtp.Port = Int32.Parse(smtpport);
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                logger.Info("Excption occured: Method name: SendEmail ", ex);
                throw ex;
            }
        }

        

    }
}
