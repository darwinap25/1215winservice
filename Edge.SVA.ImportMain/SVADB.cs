﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;

namespace Edge.SVA.ImportMain
{
    public class SVADB
    {
        string SQLConnectionStr = "";
        public SVADB(string connstr)
        {
            SQLConnectionStr = connstr;
        }

        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public DataTable GetMemeberAttributeMapping(int atype, ref string SQLSTR)
        {
            string filterstr = "";
            if (atype == 1)
            { filterstr = "Member Import Information"; }
            else if (atype == 2)
            { filterstr = "Member Import - Link"; }
            else if (atype == 3)
            { filterstr = "Member Import - Link"; }

            //string queryString = "Select ID,Description,Value,ValueType, ImportFieldName from ImportCustomerMapping ";
            string queryString = "Select CSVColumnName as ID,Description,DataType,FixedValue, XMLFieldName as ImportFieldName from CSVXMLBinding "
                  + " where ( Func = '" + filterstr + "' )";

            SQLSTR = queryString;
            using (SqlConnection sqlconn = new SqlConnection(SQLConnectionStr))
            {
                DataSet ds = new DataSet();
                try
                {
                    sqlconn.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = new SqlCommand(queryString, sqlconn);
                    adapter.Fill(ds);
                    return ds.Tables[0];
                }
                catch
                { return null; }              
            }
        }

        public DataTable GetCSVXMLMointoringRule()
        {
            string queryString = " select RuleName, Description, Func, DownloadPath, ActiveTime from CSVXMLMointoringRule H " 
               + " where  ( (isnull(H.DayFlagID, 0) in (0,-1)) or ( (isnull(H.DayFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.DayFlagID, 1, getdate(), 1) = 1) ) ) "
               + " and ( (isnull(H.WeekFlagID, 0) in (0,-1)) or ( (isnull(H.WeekFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.WeekFlagID, 2, getdate(), 1) = 1) ) ) "
               + " and ( (isnull(H.MonthFlagID, 0) in (0,-1)) or ( (isnull(H.MonthFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.MonthFlagID, 3, getdate(), 1) = 1) ) ) "
               + " and Status = 1 and DATEDIFF(dd, StartDate, getdate()) > 0 and DATEDIFF(dd, EndDate, getdate()) < 0 "
               + " and left(cast (H.ActiveTime as time),5) = left(cast(CONVERT (time, GETDATE()) as CHAR) , 5)";              
            using (SqlConnection sqlconn = new SqlConnection(SQLConnectionStr))
            {
                DataSet ds = new DataSet();
                try
                {
                    logger.Info("SQL - Opening Connection");
                    sqlconn.Open();
                    logger.Info("SQL - Connection Open");
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    
                    
                    adapter.SelectCommand = new SqlCommand(queryString, sqlconn);
                    
                    adapter.Fill(ds);
                    return ds.Tables[0];
                }
                catch (Exception ex)
                { 
                  //  return null; 
                    throw ex;
                }
            }
        }

        public string GetSegmentAction(string ordernumber)
        {
            string action = "";
            string queryString = " select [Action] from Ord_ImportMemberSegment_H where ImportMemberSegmentNumber = '" + ordernumber +"' " ;
            using (SqlConnection sqlconn = new SqlConnection(SQLConnectionStr))
            {
                
                try
                {
                    SqlCommand command = new SqlCommand(queryString, sqlconn);

                    sqlconn.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    // Call Read before accessing data.
                    while (reader.Read())
                    {
                        action = reader["Action"].ToString();
                    }

                    // Call Close when done reading.
                    reader.Close();

                    return action;
                }
                catch
                { return null; }
            }
        }

        public DataTable GetPendingCouponAdjustOrder()
        {
            string queryString = "select ImportMemberNumber from Ord_ImportMember_h where ApproveStatus = 'P' ";
            using (SqlConnection sqlconn = new SqlConnection(SQLConnectionStr))
            {
                DataSet ds = new DataSet();
                try
                {
                    sqlconn.Open();                    
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = new SqlCommand(queryString, sqlconn);
                    adapter.Fill(ds);
                    return ds.Tables[0];
                }
                catch
                { return null; }
            }
        }

        public DataTable GetPendingMemberImportSegment()
        {
            string queryString = " select ImportMemberSegmentNumber from Ord_ImportMemberSegment_h where ApproveStatus = 'P' ";
            using (SqlConnection sqlconn = new SqlConnection(SQLConnectionStr))
            {
                DataSet ds = new DataSet();
                try
                {
                    sqlconn.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = new SqlCommand(queryString, sqlconn);
                    adapter.Fill(ds);
                    return ds.Tables[0];
                }
                catch
                { return null; }
            }
        }

        public void UpdateCouponAdjustOrder(string NumberList)
        {
            string queryString = "Update Ord_ImportMember_h set ApproveStatus = 'A' where ApproveStatus = 'P' and ImportMemberNumber in (" + NumberList + ")";
            using (SqlConnection sqlconn = new SqlConnection(SQLConnectionStr))
            {
                DataSet ds = new DataSet();
                try
                {
                    sqlconn.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.UpdateCommand = new SqlCommand(queryString, sqlconn);
                    adapter.UpdateCommand.ExecuteNonQuery();
                }
                catch
                {  }
            }
        }

        public void UpdateImportMember(string NumberList)
        {
            string queryString = "Update Ord_ImportMember_h set ApproveStatus = 'A' where ApproveStatus = 'P' and ImportMemberNumber in (" + NumberList + ")";
            using (SqlConnection sqlconn = new SqlConnection(SQLConnectionStr))
            {
                DataSet ds = new DataSet();
                try
                {
                    sqlconn.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.UpdateCommand = new SqlCommand(queryString, sqlconn);
                    adapter.UpdateCommand.ExecuteNonQuery();
                }
                catch
                { }
            }
        }

        public void UpdateImportMemberSegment(string NumberList)
        {
            string queryString = "Update Ord_ImportMemberSegment_h set ApproveStatus = 'A' where ApproveStatus = 'P' and ImportMemberSegmentNumber in (" + NumberList + ")";
            using (SqlConnection sqlconn = new SqlConnection(SQLConnectionStr))
            {
                DataSet ds = new DataSet();
                try
                {
                    sqlconn.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.UpdateCommand = new SqlCommand(queryString, sqlconn);
                    adapter.UpdateCommand.ExecuteNonQuery();
                }
                catch
                { }
            }
        }

        public string GetOrderNumber()
        {
            string queryString = "declare @Number varchar(64) exec GetRefNoString 'PTransID',  @Number output  select replace(@Number, 'PTrans', '') as Number ";
            using (SqlConnection sqlconn = new SqlConnection(SQLConnectionStr))
            {
                DataSet ds = new DataSet();
                try
                {
                    sqlconn.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = new SqlCommand(queryString, sqlconn);
                    adapter.Fill(ds);
                    return ds.Tables[0].Rows[0]["Number"].ToString();
                }
                catch
                { return null; }
            }
        }

    }
}
