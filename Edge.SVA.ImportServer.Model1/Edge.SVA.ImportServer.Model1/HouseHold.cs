﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model1
{
    /// <summary>
    /// HouseHold
    /// </summary>
    [Serializable]
    public partial class HouseHold
    {
        public HouseHold()
        { }

        #region Model
        private string _householdexternalid;
        
        private HouseHoldSegments householdsegments;

        /// <summary>
        /// 主键
        /// </summary>
        [XmlAttribute("HouseHoldExternalId")]
        public string HouseHoldExternalId
        {
            set { _householdexternalid = value; }
            get { return _householdexternalid; }
        }


        [XmlElement("HouseHoldSegments")]
        public HouseHoldSegments HouseHoldSegments
        {
            get
            {
                if (householdsegments == null)
                {
                    householdsegments = new HouseHoldSegments();
                }
                return householdsegments;
            }
            set { householdsegments = value; }
        }
        #endregion Model
    }
}
