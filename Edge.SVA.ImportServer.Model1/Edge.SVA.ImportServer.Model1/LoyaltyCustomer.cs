﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model1
{
    /// <summary>
    /// LoyaltyCustomer
    /// </summary>
    [Serializable]
    public partial class LoyaltyCustomer
    {
        public LoyaltyCustomer()
        { }
        #region Model
        private string _action;
        private Retailer retailer;
        /// <summary>
        /// 主键
        /// </summary>
        [XmlAttribute("Action")]
        public string Action
        {
            set { _action = value; }
            get { return _action; }
        }

        [XmlElement("Retailer")]
        public Retailer Retailer
        {
            get
            {
                if (retailer == null)
                {
                    retailer = new Retailer();
                }
                return retailer;
            }
            set { retailer = value; }
        }
        #endregion Model
    }
}
