﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model1
{
    [Serializable]
    public partial class Segment
    {
        public Segment()
        { }

        private string _id;
        private string _status;

        [XmlAttribute("Id")]
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }

        [XmlAttribute("Status")]
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
    }
}
