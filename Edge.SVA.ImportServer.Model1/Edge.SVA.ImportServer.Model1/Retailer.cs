﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Edge.SVA.ImportServer.Model1
{
    /// <summary>
    /// Retailer
    /// </summary>
    [Serializable]
    public partial class Retailer
    {
        public Retailer()
        { }
        #region Model
        private string _id;
        private List<HouseHold> household;


        /// <summary>
        /// 主键
        /// </summary>
        [XmlAttribute("Id")]
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        [XmlElement("HouseHold")]
        public List<HouseHold> HouseHold
        {
            get
            {
                if (household == null)
                {
                    household = new List<HouseHold>();
                }
                return household;
            }
            set { household = value; }
        }

        private HouseHoldArray householdarray;

        //[XmlElement("HouseHold")]
        //public HouseHoldArray HouseHoldArray
        //{
        //    get
        //    {
        //        if (householdarray == null)
        //        {
        //            householdarray = new HouseHoldArray();
        //        }
        //        return householdarray;
        //    }
        //    set { householdarray = value; }
        //}
        

        #endregion Model
    }
}
